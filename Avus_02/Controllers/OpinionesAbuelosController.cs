﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;

namespace Avus_02.Controllers
{
    public class OpinionesAbuelosController : Controller
    {
        private readonly AvusContext _context;

        public OpinionesAbuelosController(AvusContext context)
        {
            _context = context;
        }

        // GET: OpinionesAbuelos
        public async Task<IActionResult> Index()
        {
            var avusContext = _context.OpinionesAbuelo.Include(o => o.IdAbueloNavigation).Include(o => o.IdEstudianteNavigation);
            return View(await avusContext.ToListAsync());
        }

        // GET: OpinionesAbuelos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesAbuelo = await _context.OpinionesAbuelo
                .Include(o => o.IdAbueloNavigation)
                .Include(o => o.IdEstudianteNavigation)
                .FirstOrDefaultAsync(m => m.IdOpinionesAbuelo == id);
            if (opinionesAbuelo == null)
            {
                return NotFound();
            }

            return View(opinionesAbuelo);
        }

        // GET: OpinionesAbuelos/Create
        public IActionResult Create()
        {
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo");
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante");
            return View();
        }

        // POST: OpinionesAbuelos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdOpinionesAbuelo,Opinion,IdEstudiante,IdAbuelo")] OpinionesAbuelo opinionesAbuelo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(opinionesAbuelo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesAbuelo.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesAbuelo.IdEstudiante);
            return View(opinionesAbuelo);
        }

        // GET: OpinionesAbuelos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesAbuelo = await _context.OpinionesAbuelo.FindAsync(id);
            if (opinionesAbuelo == null)
            {
                return NotFound();
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesAbuelo.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesAbuelo.IdEstudiante);
            return View(opinionesAbuelo);
        }

        // POST: OpinionesAbuelos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdOpinionesAbuelo,Opinion,IdEstudiante,IdAbuelo")] OpinionesAbuelo opinionesAbuelo)
        {
            if (id != opinionesAbuelo.IdOpinionesAbuelo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(opinionesAbuelo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OpinionesAbueloExists(opinionesAbuelo.IdOpinionesAbuelo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesAbuelo.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesAbuelo.IdEstudiante);
            return View(opinionesAbuelo);
        }

        // GET: OpinionesAbuelos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesAbuelo = await _context.OpinionesAbuelo
                .Include(o => o.IdAbueloNavigation)
                .Include(o => o.IdEstudianteNavigation)
                .FirstOrDefaultAsync(m => m.IdOpinionesAbuelo == id);
            if (opinionesAbuelo == null)
            {
                return NotFound();
            }

            return View(opinionesAbuelo);
        }

        // POST: OpinionesAbuelos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var opinionesAbuelo = await _context.OpinionesAbuelo.FindAsync(id);
            _context.OpinionesAbuelo.Remove(opinionesAbuelo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OpinionesAbueloExists(int id)
        {
            return _context.OpinionesAbuelo.Any(e => e.IdOpinionesAbuelo == id);
        }
    }
}
