﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;

namespace Avus_02.Controllers
{
    public class OpinionesEstudiantesController : Controller
    {
        private readonly AvusContext _context;

        public OpinionesEstudiantesController(AvusContext context)
        {
            _context = context;
        }

        // GET: OpinionesEstudiantes
        public async Task<IActionResult> Index()
        {
            var avusContext = _context.OpinionesEstudiante.Include(o => o.IdAbueloNavigation).Include(o => o.IdEstudianteNavigation);
            return View(await avusContext.ToListAsync());
        }

        // GET: OpinionesEstudiantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesEstudiante = await _context.OpinionesEstudiante
                .Include(o => o.IdAbueloNavigation)
                .Include(o => o.IdEstudianteNavigation)
                .FirstOrDefaultAsync(m => m.IdOpinionesEstudiante == id);
            if (opinionesEstudiante == null)
            {
                return NotFound();
            }

            return View(opinionesEstudiante);
        }

        // GET: OpinionesEstudiantes/Create
        public IActionResult Create()
        {
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo");
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante");
            return View();
        }

        // POST: OpinionesEstudiantes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdOpinionesEstudiante,Opinion,IdAbuelo,IdEstudiante")] OpinionesEstudiante opinionesEstudiante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(opinionesEstudiante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesEstudiante.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesEstudiante.IdEstudiante);
            return View(opinionesEstudiante);
        }

        // GET: OpinionesEstudiantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesEstudiante = await _context.OpinionesEstudiante.FindAsync(id);
            if (opinionesEstudiante == null)
            {
                return NotFound();
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesEstudiante.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesEstudiante.IdEstudiante);
            return View(opinionesEstudiante);
        }

        // POST: OpinionesEstudiantes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdOpinionesEstudiante,Opinion,IdAbuelo,IdEstudiante")] OpinionesEstudiante opinionesEstudiante)
        {
            if (id != opinionesEstudiante.IdOpinionesEstudiante)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(opinionesEstudiante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OpinionesEstudianteExists(opinionesEstudiante.IdOpinionesEstudiante))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", opinionesEstudiante.IdAbuelo);
            ViewData["IdEstudiante"] = new SelectList(_context.Estudiante, "IdEstudiante", "IdEstudiante", opinionesEstudiante.IdEstudiante);
            return View(opinionesEstudiante);
        }

        // GET: OpinionesEstudiantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opinionesEstudiante = await _context.OpinionesEstudiante
                .Include(o => o.IdAbueloNavigation)
                .Include(o => o.IdEstudianteNavigation)
                .FirstOrDefaultAsync(m => m.IdOpinionesEstudiante == id);
            if (opinionesEstudiante == null)
            {
                return NotFound();
            }

            return View(opinionesEstudiante);
        }

        // POST: OpinionesEstudiantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var opinionesEstudiante = await _context.OpinionesEstudiante.FindAsync(id);
            _context.OpinionesEstudiante.Remove(opinionesEstudiante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OpinionesEstudianteExists(int id)
        {
            return _context.OpinionesEstudiante.Any(e => e.IdOpinionesEstudiante == id);
        }
    }
}
