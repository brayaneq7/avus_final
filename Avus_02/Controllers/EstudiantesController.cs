﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Collections;
using System.IO;

namespace Avus_02.Controllers
{
    public class EstudiantesController : Controller
    {
        private readonly AvusContext _context;
        private readonly IHostingEnvironment hostingEnvironment;

        public EstudiantesController(AvusContext context, IHostingEnvironment environment)
        {
            _context = context;
            hostingEnvironment = environment;

        }

        // GET: Estudiantes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Estudiante.ToListAsync());
        }

        // GET: Estudiantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estudiante = await _context.Estudiante
                .FirstOrDefaultAsync(m => m.IdEstudiante == id);
            if (estudiante == null)
            {
                return NotFound();
            }

            return View(estudiante);
        }

        // GET: Estudiantes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Estudiantes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdEstudiante,Email,Password,Nombre,Apellidos,Dni,Descripcion,FotoPerfil,FechaNacimiento,Sexo,Telefono,Estudios")] Estudiante estudiante, SubirFoto foto, String Nacionalidad)
        {
            string[] Paises = new string[]
       {
                "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
           };
            estudiante.Nacionalidad = Array.IndexOf(Paises, Nacionalidad);
            if (ModelState.IsValid)
            {
                //comprobar que el estudiante no existe
                var comprobar_estudiante = _context.Estudiante.Where(predicate: s => s.Email.Equals(estudiante.Email)).FirstOrDefault();

                if (comprobar_estudiante != null)
                {
                    return NotFound();
                }
                else
                {
                    //Encrypt password al crear un nuevo estudiante.
                    estudiante.Password = Encriptar.ComputeSha256Hash(estudiante.Password);
                    _context.Add(estudiante);
                    await _context.SaveChangesAsync();
                    SubirImagenesController.Create2(foto, hostingEnvironment, estudiante.IdEstudiante, 2);
                    return RedirectToAction("Index", "Login");
                }

            }
            // return View(estudiante);
            return RedirectToAction("Index", "Login");
        }

        // GET: Estudiantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estudiante = await _context.Estudiante.FindAsync(id);
            if (estudiante == null)
            {
                return NotFound();
            }
            return View(estudiante);
        }

        // POST: Estudiantes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdEstudiante,Email,Password,Nombre,Apellidos,Dni,Descripcion,Nacionalidad,FotoPerfil,FechaNacimiento,Sexo,Telefono,Estudios")] Estudiante estudiante)
        {
            if (id != estudiante.IdEstudiante)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(estudiante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstudianteExists(estudiante.IdEstudiante))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(estudiante);
        }

        // GET: Estudiantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estudiante = await _context.Estudiante
                .FirstOrDefaultAsync(m => m.IdEstudiante == id);
            if (estudiante == null)
            {
                return NotFound();
            }

            return View(estudiante);
        }

        // POST: Estudiantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var estudiante = await _context.Estudiante.FindAsync(id);
            _context.Estudiante.Remove(estudiante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EstudianteExists(int id)
        {
            return _context.Estudiante.Any(e => e.IdEstudiante == id);
        }

        //FormEstudiante
        public IActionResult FormEstudiante()
        {
                
            
                return View();
            
              
        }
      

        public IActionResult Busqueda()
        {
            if (getEstudiante() != null)
            {
                return View(getEstudiante());
            }
            else
            {
                return RedirectToAction("Logout", "Login");
            }
        }

        private Estudiante getEstudiante()
        {
            var id = HttpContext.Session.GetInt32("id");
            var tipo = HttpContext.Session.GetString("tipo");
            if (tipo != null && id != null)
            {
                if (tipo.Equals("estudiante"))
            {
                if (id == null) { return null; }

                var estudiante = _context.Estudiante.Where(predicate: s => s.IdEstudiante == id).FirstOrDefault();

                if (estudiante == null) { return null; }

                return estudiante;

            }
            HttpContext.Session.Clear();
            return null;
            }
            else { return null; }

        }
        public IActionResult Solicitudes()
        {
            ViewData["Solicitud2Class"] = "active";
            var estudiante = getEstudiante ();
            if (getEstudiante() == null) { return NotFound(); }
            else {
                var id = HttpContext.Session.GetInt32("id");
                List<Solicitud> solicitudes = new List<Solicitud>();
                List<Habitacion> listadehabitaciones = new List<Habitacion>();
                List<Abuelo> abuelos = new List<Abuelo>();
                ArrayList nombreshabitaciones = new ArrayList();
                //Prueba Join var lista =  _context.Habitacion.GroupJoin(_context.Solicitud, habitaciones => habitaciones.IdHabitacion, solicitud => solicitud.IdHabitacion, (habitaciones, solicitud) => new { habitaciones, solicitud }).Where(habit => habit.habitaciones.IdAbuelo == id).ToList();
                var listasolicitudes = _context.Solicitud.Where(predicate: s => s.IdEstudiante == id).ToList();
                if (listasolicitudes != null)
                {
                    foreach (Solicitud solicitud in listasolicitudes)
                    {
                        solicitudes.Add(solicitud);
                        var match = _context.Habitacion.Where(predicate: s => s.IdHabitacion == solicitud.IdHabitacion).ToList();
                        if (match != null)
                        {
                            foreach (Habitacion habitacion in match)
                            {
                                if (!listadehabitaciones.Contains(habitacion))
                                {
                                    listadehabitaciones.Add(habitacion);
                                    nombreshabitaciones.Add(ordenarnumhabitacion(habitacion.IdHabitacion));
                                }
                            }
                        }
                    }
                }

                ViewBag.ListaHabitaciones = listadehabitaciones;
                ViewBag.nombreshabitaciones = nombreshabitaciones;
                ViewBag.ListaSolicitudesEstudiante = solicitudes;

                return View(getEstudiante()); }

        }
        public string ordenarnumhabitacion(int idhabitacion) {
           
            var ordenarporid = _context.Habitacion.SingleOrDefault(predicate: s => s.IdHabitacion == idhabitacion);

            var idabuelo = _context.Abuelo.SingleOrDefault(predicate: s => s.IdAbuelo == ordenarporid.IdAbuelo);

            var habitacionesabuelo = _context.Habitacion.Where(predicate: s => s.IdAbuelo == idabuelo.IdAbuelo).ToList();

            int contador = 1;
            foreach (Habitacion habitacion in habitacionesabuelo) {
                if (habitacion.IdHabitacion == idhabitacion) {
                    return idabuelo.Nombre + " "+idabuelo.Apellidos+" Room - "+contador;
                }
                contador++;
            }
            return idabuelo.Nombre + " " + idabuelo.Apellidos + " Room - 4";
        }
        public IActionResult cambiarSolicitud()
        {

            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));
            try
            {
                solicitud.FechaUltimoDia = DateTime.Today;
                _context.Update(solicitud);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();
            }
            return RedirectToAction("Solicitudes", "Estudiantes");
        }
        public IActionResult BorrarSolicitud()
        {
            //Recibiremos la id de la solicitud y la buscamos en la bbdd.
            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));

            _context.Solicitud.Remove(solicitud);
            _context.SaveChangesAsync();

            return RedirectToAction("Solicitudes", "Estudiantes");
        }
        // EDITAR ESTUDIANTES
        public async Task<IActionResult> EditarEstudiante()
        {
            var id = HttpContext.Session.GetInt32("id");

            if (id == null)
            {
                return NotFound();
            }

            var estudiante = await _context.Estudiante.FindAsync(id);
            if (estudiante == null)
            {
                return NotFound();
            }
            return View(estudiante);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditarEstudiante([Bind("IdEstudiante,Email,Password,Nombre,Apellidos,Dni,Descripcion,Nacionalidad,FotoPerfil,FechaNacimiento,Sexo,Telefono,Estudios")] Estudiante estudiante)
        {
            var id = HttpContext.Session.GetInt32("id");

            if (id != estudiante.IdEstudiante)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    estudiante.Password = Encriptar.ComputeSha256Hash(estudiante.Password);
                    _context.Update(estudiante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstudianteExists(estudiante.IdEstudiante))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Solicitudes", "Estudiantes");
            }
            return View(estudiante);
        }

        [HttpPost]
        public IActionResult ResultadoBusqueda(string Pais,string Comunidad,string Provincia,string Localidad,string CodigoPostal,string Sexo,string Disponible, string Mascotas, string Invitados, string Fumadores, string Wifi)
        {

            var estudiante = getEstudiante();
            if (estudiante == null) { return NotFound(); }
            else
            {

                var id = HttpContext.Session.GetInt32("id");
                int disp = 0;
                int masc = 0;
                int invi = 0;
                int fuma = 0;
                int wi = 0;
                List<Abuelo> abueloslista = new List<Abuelo>();
                List<Habitacion> habitacioneslista = new List<Habitacion>();
                ArrayList idabuelos = new ArrayList();

                if (Disponible == "on") {
                    disp = 1;
                }

                if (Mascotas == "on") {
                    masc = 1;
                }
                if (Invitados == "on")
                {
                    invi = 1;
                }
                if (Fumadores == "on")
                {
                    fuma = 1;
                }
                if (Wifi == "on")
                {
                    wi = 1;
                }
               var habitaciones = _context.Habitacion.Where(predicate: s => s.Disponible == disp && s.Mascotas == masc && s.Invitados == invi && s.Fumar == fuma && s.Wifi == wi).ToList();
                if (habitaciones != null)
                {
                    foreach (Habitacion posicionhabitacion in habitaciones)
                    {
                        habitacioneslista.Add(posicionhabitacion);
                        if (!idabuelos.Contains(posicionhabitacion.IdAbuelo))
                        {
                            var abuelo = _context.Abuelo.Where(predicate: s => s.IdAbuelo == posicionhabitacion.IdAbuelo).FirstOrDefault();
                            abueloslista.Add(abuelo);
                            idabuelos.Add(posicionhabitacion.IdAbuelo);
                        }
                    }
            }
                ViewBag.ListaAbuelos = abueloslista;
                ViewBag.ListaHabitaciones = habitacioneslista;
                return View(estudiante);
            }
        }
        //con esto cargamos directamente la imagen 
        public ActionResult ImageAbuelo()
        {
            string id = Request.Path.Value.Split('/').Last();
            //var dir = Server.MapPath("/Images");
            var path = Path.Combine("/Images/perfil", "A" + id + ".jpg"); //validate the path for security or use other means to generate the path.
            return base.File(path, "image/jpeg");
        }

        //con esto cargamos directamente la imagen 
        public ActionResult ImageHabitacion()
        {
            string id = Request.Path.Value.Split('/').Last();
            //var dir = Server.MapPath("/Images");
            var path = Path.Combine("/Images/habitacion", id + ".jpg"); //validate the path for security or use other means to generate the path.
            return base.File(path, "image/jpeg");
        }
        public IActionResult PerfilAbuelo() {

            int? idestudiante = HttpContext.Session.GetInt32("id");
            string id = Request.Path.Value.Split('/').Last();
            List<Abuelo> avus = new List<Abuelo>();
            List<Solicitud> listasolicitudes = new List<Solicitud>();
            var abuelo = _context.Abuelo.Where(predicate: s => s.IdAbuelo == Int32.Parse(id)).FirstOrDefault();

            avus.Add(abuelo);

            var habitaciones = _context.Habitacion.Where(predicate: s => s.IdAbuelo == abuelo.IdAbuelo).ToList();

            foreach (Habitacion habitacion in habitaciones) {
                var solicitudes = _context.Solicitud.Where(predicate: s => s.IdHabitacion == habitacion.IdHabitacion && s.IdEstudiante == idestudiante).FirstOrDefault();
                listasolicitudes.Add(solicitudes);
            }

                foreach (Solicitud solicitud in listasolicitudes)
                {
                    if (solicitud != null)
                    {
                        if (solicitud.Aceptacion != 0)
                        {
                            ViewBag.Email = abuelo.Email;
                            ViewBag.Telefono = abuelo.Telefono;
                            break;
                        }
                        else
                        {
                            ViewBag.Email = " - - ";
                            ViewBag.Telefono = " - - ";
                        }
                    }
                    else {
                        ViewBag.Email = " - - ";
                        ViewBag.Telefono = " - - ";
                    }
                }
 



            var opiniones = _context.OpinionesAbuelo.Where(predicate: s => s.IdAbuelo == abuelo.IdAbuelo).ToList();

            ViewBag.Abuelo = avus;
            ViewBag.ListaHabitaciones = habitaciones;
            ViewBag.OpinionesAbuelo = opiniones;

            return View();
        }
        public IActionResult VerPerfilMio()
        {
            int? id = HttpContext.Session.GetInt32("id");
            List<Estudiante> estudiantes = new List<Estudiante>();
            var estudiante = _context.Estudiante.SingleOrDefault(predicate: s => s.IdEstudiante == id);
            estudiantes.Add(estudiante);
            ViewBag.estudiante = estudiantes;
            var opiniones = _context.OpinionesEstudiante.Where(predicate: s => s.IdEstudiante == id);
            ViewBag.listaopiniones = opiniones;
            return View(estudiante);
        }
        public IActionResult CrearSolicitud([Bind("IdSolicitud,FechaSolicitud,Aceptacion,FechaPrimerDia,FechaUltimoDia,IdEstudiante,IdHabitacion")] Solicitud solicitud)
        {
            string idhabitacion = Request.Path.Value.Split('/').Last();
            var idEstudiante = HttpContext.Session.GetInt32("id");
            var comprobarsolicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdHabitacion == Int32.Parse(idhabitacion)&& s.IdEstudiante == idEstudiante);
            if (comprobarsolicitud != null)
            {
                return RedirectToAction(nameof(Busqueda));
            }
            else
            {
                solicitud.IdHabitacion = Int32.Parse(idhabitacion);
                solicitud.IdEstudiante = idEstudiante;
                solicitud.Aceptacion = 0;
                solicitud.FechaSolicitud = DateTime.Today;

                _context.Add(solicitud);
                _context.SaveChangesAsync();


                return RedirectToAction(nameof(Busqueda));
            }
        }

    }

}
