﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;
using static System.Net.WebRequestMethods;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Collections;

namespace Avus_02.Controllers
{
    public class AbuelosController : Controller
    {
        private readonly AvusContext _context;
        private readonly IHostingEnvironment hostingEnvironment;


        public AbuelosController(AvusContext context, IHostingEnvironment environment)
        {
            _context = context;
            hostingEnvironment = environment;
        }


        

        // GET: Abuelos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Abuelo.ToListAsync());
        }

        // GET: Abuelos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var abuelo = await _context.Abuelo
                .FirstOrDefaultAsync(m => m.IdAbuelo == id);
            if (abuelo == null)
            {
                return NotFound();
            }

            return View(abuelo);
        }

        // GET: Abuelos/Create
        public IActionResult Create()
        {
            return View();
        }
        //Email,Password,Nombre,Apellidos,Dni,Descripcion,Sexo,Telefono,Direccion,CodigoPostal
        public int[] DevolverPaises(string Pais, string Comunidad, string Localidad)
        {
             string[] Paises = new string[]
        {
                 "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
            };
             string[] comunidad = new string[] {
     "Andalucía", "Aragón", "Asturias", "Canarias", "Cantabria", "Castilla La Mancha", "Castilla y León", "Cataluña", "Comunidad de Madrid", "Comunidad Valenciana",
     "Extremadura", "Galicia", "Islas Baleares", "La Rioja", "Murcia", "Navarra", "País Vasco" };
             string[] localidadAndalucia = new string[] { "Almería", "Cádiz", "Córdoba", "Granada", "Huelva", "Jaén", "Málaga", "Sevilla" };
             string[] localidadAragon = new string[] { "Huesca", "Teruel", "Zaragoza" };
             string[] localidadAsturias = new string[] { "Asturias" };
             string[] localidadCanarias = new string[] { "Las Palmas", "Santa Cruz de Tenerife" };
             string[] localidadCantabria = new string[] { "Cantabria" };
             string[] localidadCastillaMancha = new string[] { "Albacete", "Ciudad Real", "Cuenca", "Guadalajara", "Toledo" };
             string[] localidadCastillaLeon = new string[] { "Ávila", "Burgos", "León", "Palencia", "Salamanca", "Segovia", "Soria", "Valladolid ", "Zamora" };
             string[] localidadCataluña = new string[] { "Barcelona", "Girona", "Lleida ", "Tarragona" };
             string[] localidadValenciana = new string[] { "Alicante", "Castellón ", "Valencia" };
             string[] localidadMadrid = new string[] { "Madrid" };
             string[] localidadExtremadura = new string[] { "Badajoz ", "Cáceres" };
             string[] localidadGalicia = new string[] { "A Coruña", "Lugo", "Ourense", "Pontevedra" };
             string[] localidadIslasBaleares = new string[] { "Cabrera ", "Formentera", "Ibiza", "Mallorca", "Menorca" };
             string[] localidadRioja = new string[] { "La Rioja" };
             string[] localidadMurcia = new string[] { "Murcia" };
             string[] localidadNavarra = new string[] { "Navarra" };
             string[] localidadVasco = new string[] { "Álava", "Guipuzcoa", "Vizcaya" };
             int numerolocalidad = 0;
                         switch (Comunidad)
                         {
 
                 case "Andalucía":
 numerolocalidad = Array.IndexOf(localidadAndalucia, Localidad);
                                 break;
             
                             case "Aragón":
 numerolocalidad = Array.IndexOf(localidadAragon, Localidad);
             
                                 break;
                             case "Asturias":
 numerolocalidad = Array.IndexOf(localidadAsturias, Localidad);
             
                                 break;
                             case "Canarias":
 numerolocalidad = Array.IndexOf(localidadCanarias, Localidad);
                                 break;
                             case "Cantabria":
 numerolocalidad = Array.IndexOf(localidadCantabria, Localidad);
                                 break;
                             case "Castilla La Mancha":
 numerolocalidad = Array.IndexOf(localidadCastillaMancha, Localidad);
                                 break;
                             case "Castilla y León":
 numerolocalidad = Array.IndexOf(localidadCastillaLeon, Localidad);
                                 break;
                             case "Cataluña":
 numerolocalidad = Array.IndexOf(localidadCataluña, Localidad);
                                 break;
                             case "Comunidad de Madrid":
 numerolocalidad = Array.IndexOf(localidadMadrid, Localidad);
                                 break;
                             case "Comunidad Valenciana":
 numerolocalidad = Array.IndexOf(localidadValenciana, Localidad);
                                 break;
                             case "Extremadura":
 numerolocalidad = Array.IndexOf(localidadExtremadura, Localidad);
                                 break;
                             case "Galicia":
 numerolocalidad = Array.IndexOf(localidadGalicia, Localidad);
                                 break;
                             case "Islas Baleares":
 numerolocalidad = Array.IndexOf(localidadIslasBaleares, Localidad);
                                 break;
                             case "La Rioja":
 numerolocalidad = Array.IndexOf(localidadRioja, Localidad);
                                 break;
                             case "Murcia":
 numerolocalidad = Array.IndexOf(localidadMurcia, Localidad);
                                 break;
                             case "Navarra":
 numerolocalidad = Array.IndexOf(localidadNavarra, Localidad);
                                 break;
                             case "País Vasco":
 numerolocalidad = Array.IndexOf(localidadVasco, Localidad);
                                 break;
             
             default:
                     break;
             
                         }
             int paisentero = Array.IndexOf(Paises, Pais);
             int comunidadentero = Array.IndexOf(comunidad, Comunidad);
             int[] arraynumeros = new int[] { paisentero, comunidadentero, numerolocalidad };
             return arraynumeros;
        }
    // POST: Abuelos/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdAbuelo,Email,Password,Nombre,Apellidos,Dni,Descripcion,Sexo,Telefono,Direccion,CodigoPostal,FechaNacimiento")] Abuelo abuelo, SubirFoto foto, string Pais, string Comunidad, string Localidad)
        {
            int[] posicion = DevolverPaises(Pais, Comunidad, Localidad);
            abuelo.Nacionalidad = 1;
            abuelo.Pais = posicion[0];
            abuelo.Comunidad = posicion[1];
            abuelo.Localidad = posicion[2];
            //Comprobamos que el abuelo no esta creado
            if (ModelState.IsValid)
            {

                var comprobar_abuelo = _context.Abuelo.Where(predicate: s => s.Email.Equals(abuelo.Email)).FirstOrDefault();

                if (comprobar_abuelo != null)
                {
                    return NotFound();
                }
                else
                {
                    //Encriptar password abus
                    abuelo.Password = Encriptar.ComputeSha256Hash(abuelo.Password);
                    _context.Add(abuelo);
                    await _context.SaveChangesAsync();

                    SubirImagenesController.Create2(foto, hostingEnvironment, abuelo.IdAbuelo ,1);


                   return RedirectToAction("Index", "Login"); 
                }

            }
            // return View(abuelo);
            return RedirectToAction("Index", "Login");
        }

        // GET: Abuelos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var abuelo = await _context.Abuelo.FindAsync(id);
            if (abuelo == null)
            {
                return NotFound();
            }
            return View(abuelo);
        }



        // POST: Abuelos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdAbuelo,Email,Password,Nombre,Apellidos,Dni,Descripcion,Nacionalidad,FotoPerfil,FechaNacimiento,Sexo,Telefono,Pais,Comunidad,Localidad,Direccion,CodigoPostal")] Abuelo abuelo)
        {
            if (id != abuelo.IdAbuelo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(abuelo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AbueloExists(abuelo.IdAbuelo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(abuelo);
        }

        // GET: Abuelos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var abuelo = await _context.Abuelo
                .FirstOrDefaultAsync(m => m.IdAbuelo == id);
            if (abuelo == null)
            {
                return NotFound();
            }

            return View(abuelo);
        }

        // POST: Abuelos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var abuelo = await _context.Abuelo.FindAsync(id);
            _context.Abuelo.Remove(abuelo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AbueloExists(int id)
        {
            return _context.Abuelo.Any(e => e.IdAbuelo == id);
        }

        public IActionResult FormAbuelo()
        {
            return View();
        }
        
        

        private Abuelo getAbuelo()
        {
            var id = HttpContext.Session.GetInt32("id");
            var tipo = HttpContext.Session.GetString("tipo");
            if (tipo != null && id != null)
            {
                if (tipo.Equals("abuelo"))
                {
                    if (id == null) { return null; }

                    var abuelo = _context.Abuelo.Where(predicate: s => s.IdAbuelo == id).FirstOrDefault();

                    if (abuelo == null) { return null; }

                    return abuelo;

                }
                HttpContext.Session.Clear();
                return null;
            }
            else { return null; }
        }
        public IActionResult Solicitudes()
        {
            ViewData["SolicitudClass"] = "active";
            ViewData["HabitacionClass"] = " ";
            var abuelo = getAbuelo();
            if (abuelo == null) { return NotFound(); }
            else
            {

                var id = HttpContext.Session.GetInt32("id");
                List<Solicitud> solicitudes = new List<Solicitud>();
                List<Estudiante> estudiantes = new List<Estudiante>();
                ArrayList nombrehabitacion = new ArrayList();
                //Prueba Join var lista =  _context.Habitacion.GroupJoin(_context.Solicitud, habitaciones => habitaciones.IdHabitacion, solicitud => solicitud.IdHabitacion, (habitaciones, solicitud) => new { habitaciones, solicitud }).Where(habit => habit.habitaciones.IdAbuelo == id).ToList();
                var listahabitaciones = _context.Habitacion.Where(predicate: s => s.IdAbuelo == id).ToList();
                if (listahabitaciones != null)
                {
                    foreach (Habitacion habitacion in listahabitaciones)
                    {
                        var solicitud = _context.Solicitud.Where(predicate: s => s.IdHabitacion == habitacion.IdHabitacion).ToList();
                        if (solicitud != null)
                        {
                            foreach (Solicitud solicitutUnitaria in solicitud)
                            {
                                if (solicitutUnitaria != null)
                                {
                                    solicitudes.Add(solicitutUnitaria);
                                    nombrehabitacion.Add(ordenarnumhabitacion(habitacion.IdHabitacion, solicitutUnitaria.IdEstudiante));
                                    if (solicitudes.Count() > 0)
                                    {

                                        var estudiante = _context.Estudiante.SingleOrDefault(predicate: s => s.IdEstudiante == solicitutUnitaria.IdEstudiante);


                                        if (estudiante != null && !estudiantes.Contains(estudiante))
                                        {

                                            estudiantes.Add(estudiante);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                ViewBag.ListaSolictudes = solicitudes;
                ViewBag.ListaEstudiantes = estudiantes;
                ViewBag.Listanombres = nombrehabitacion;

                return View(abuelo);

            }


        }



        public string ordenarnumhabitacion(int idhabitacion, int? idestudiante)
        {

            var ordenarporid = _context.Habitacion.SingleOrDefault(predicate: s => s.IdHabitacion == idhabitacion);

            var estudiante = _context.Estudiante.SingleOrDefault(predicate: s => s.IdEstudiante == idestudiante);

            var idabuelo = _context.Abuelo.SingleOrDefault(predicate: s => s.IdAbuelo == ordenarporid.IdAbuelo);

            var habitacionesabuelo = _context.Habitacion.Where(predicate: s => s.IdAbuelo == idabuelo.IdAbuelo).ToList();

            int contador = 1;
            foreach (Habitacion habitacion in habitacionesabuelo)
            {
                if (habitacion.IdHabitacion == idhabitacion)
                {
                    return estudiante.Nombre + " " + estudiante.Apellidos + " Room - " + contador;
                }
                contador++;
            }
            return idabuelo.Nombre + " " + idabuelo.Apellidos + " Room - 4";
        }
        //con esto cargamos directamente la imagen 
        public ActionResult Image()
        {
            string id = Request.Path.Value.Split('/').Last();
            //var dir = Server.MapPath("/Images");
            var path = Path.Combine("/Images/perfil", "E" + id + ".jpg"); //validate the path for security or use other means to generate the path.
            return base.File(path, "image/jpeg");
        }

        public static string ImageHabitaciones(int IdHabitacion)
        {

            var path = Path.Combine("/images/habitacion/",IdHabitacion + ".jpg"); //validate the path for security or use other means to generate the path.
            return path;
        }
        //aqui creamos los diferentes tipos de aceptar
        //Primera Comprobación
        public IActionResult cambiarSolicitudAceptar()
        {

            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));
            try
            {
                solicitud.Aceptacion = 1;
                _context.Update(solicitud);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();
            }
            return RedirectToAction("Solicitudes", "Abuelos");


        }
        //Segunda comprobacion
        public IActionResult cambiarSolicitudProceso()
        {

            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));
            try
            {
                solicitud.FechaPrimerDia = DateTime.Today;
                _context.Update(solicitud);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();
            }
            return RedirectToAction("Solicitudes", "Abuelos");
        }
        //Ultima comprobación
        public IActionResult cambiarSolicitud()
        {

            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));
            try
            {
                solicitud.FechaUltimoDia = DateTime.Today;
                _context.Update(solicitud);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();
            }
            return RedirectToAction("Solicitudes", "Abuelos");
        }

        //Borrar solicitudes
        public IActionResult BorrarSolicitud()
        {
            //Recibiremos la id de la solicitud y la buscamos en la bbdd.
            string id = Request.Path.Value.Split('/').Last();
            var solicitud = _context.Solicitud.SingleOrDefault(predicate: s => s.IdSolicitud == Int32.Parse(id));

            _context.Solicitud.Remove(solicitud);
            _context.SaveChangesAsync();

            return RedirectToAction("Solicitudes", "Abuelos");
        }

        //Controlador para ver el perfil del usuario
        public IActionResult PerfilEstudiante()
        {
            string id = Request.Path.Value.Split('/').Last();
            List<Estudiante> estudiantes = new List<Estudiante>();
            var estudiante = _context.Estudiante.SingleOrDefault(predicate: s => s.IdEstudiante == Int32.Parse(id));
            estudiantes.Add(estudiante);
            ViewBag.estudiante = estudiantes;
            var opiniones = _context.OpinionesEstudiante.Where(predicate: s => s.IdEstudiante == Int32.Parse(id));
            ViewBag.listaopiniones = opiniones;
            return View();
        }
        
         
            public IActionResult VerPerfilMio()
            {
                int? id = HttpContext.Session.GetInt32("id");
                List<Abuelo> avus = new List<Abuelo>();
                var abuelo = _context.Abuelo.Where(predicate: s => s.IdAbuelo == id).FirstOrDefault();
                avus.Add(abuelo);
                var habitaciones = _context.Habitacion.Where(predicate: s => s.IdAbuelo == abuelo.IdAbuelo).ToList();
                var opiniones = _context.OpinionesAbuelo.Where(predicate: s => s.IdAbuelo == abuelo.IdAbuelo).ToList();

                ViewBag.Abuelo = avus;
                ViewBag.ListaHabitaciones = habitaciones;
                ViewBag.OpinionesAbuelo = opiniones;

                return View(abuelo);
            }
            //Controlador para añadir unae opinions
            public IActionResult AddOpinion([Bind("IdOpinionesEstudiante,Opinion,IdAbuelo,IdEstudiante")] OpinionesEstudiante opinionesEstudiante)
        {
            var idabuelo = HttpContext.Session.GetInt32("id");
            opinionesEstudiante.IdAbuelo = idabuelo;
            _context.Add(opinionesEstudiante);
            _context.SaveChangesAsync();
            return RedirectToAction("Solicitudes", "Abuelos");
        }

        public IActionResult Habitaciones()
        {
            ViewData["SolicitudClass"] = " ";
            ViewData["HabitacionClass"] = "active";

            if (getAbuelo() == null) { return NotFound(); }
            else {
                //Coge la id del abuelo y coge su lista de habitaciones para pasarla a la View
                int? id = HttpContext.Session.GetInt32("id");
                List<Habitacion> listahabitaciones = new List<Habitacion>();
                var habitacion = _context.Habitacion.Where(predicate: s => s.IdAbuelo == id).ToList();
                foreach (var habitaciones in habitacion) {
                    listahabitaciones.Add(habitaciones);
                }
                ViewBag.listaHabitaciones = listahabitaciones;
                return View(getAbuelo());
            }
        }

        public IActionResult Ajustes()
        {
            ViewData["SolicitudClass"] = " ";
            ViewData["HabitacionClass"] = " ";

            if (getAbuelo() == null) { return NotFound(); }
            else { return View(getAbuelo()); }
        }
    
        // GET: Abuelos/Edit/5
        public async Task<IActionResult> EditarAbuelo()
        {
            var id = HttpContext.Session.GetInt32("id");

            if (id == null)
            {
                return NotFound();
            }

            var abuelo = await _context.Abuelo.FindAsync(id);
            if (abuelo == null)
            {
                return NotFound();
            }
            return View(abuelo);
        }



        // POST: Abuelos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditarAbuelo([Bind("IdAbuelo,Email,Password,Nombre,Apellidos,Dni,Descripcion,Nacionalidad,FotoPerfil,FechaNacimiento,Sexo,Telefono,Pais,Comunidad,Localidad,Direccion,CodigoPostal")] Abuelo abuelo)
        {
            var id = HttpContext.Session.GetInt32("id");

            if (id != abuelo.IdAbuelo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    abuelo.Password = Encriptar.ComputeSha256Hash(abuelo.Password);

                    _context.Update(abuelo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AbueloExists(abuelo.IdAbuelo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Solicitudes", "Abuelos");
            }
            return View(abuelo);
        }


    }

}
