﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;

namespace Avus_02.Controllers
{
    public class FotoHabitacionesController : Controller
    {
        private readonly AvusContext _context;

        public FotoHabitacionesController(AvusContext context)
        {
            _context = context;
        }

        // GET: FotoHabitaciones
        public async Task<IActionResult> Index()
        {
            var avusContext = _context.FotoHabitacion.Include(f => f.IdHabitacionNavigation);
            return View(await avusContext.ToListAsync());
        }

        // GET: FotoHabitaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fotoHabitacion = await _context.FotoHabitacion
                .Include(f => f.IdHabitacionNavigation)
                .FirstOrDefaultAsync(m => m.IdFotoHabitacion == id);
            if (fotoHabitacion == null)
            {
                return NotFound();
            }

            return View(fotoHabitacion);
        }

        // GET: FotoHabitaciones/Create
        public IActionResult Create()
        {
            ViewData["IdHabitacion"] = new SelectList(_context.Habitacion, "IdHabitacion", "IdHabitacion");
            return View();
        }

        // POST: FotoHabitaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdFotoHabitacion,Foto,IdHabitacion")] FotoHabitacion fotoHabitacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fotoHabitacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdHabitacion"] = new SelectList(_context.Habitacion, "IdHabitacion", "IdHabitacion", fotoHabitacion.IdHabitacion);
            return View(fotoHabitacion);
        }

        // GET: FotoHabitaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fotoHabitacion = await _context.FotoHabitacion.FindAsync(id);
            if (fotoHabitacion == null)
            {
                return NotFound();
            }
            ViewData["IdHabitacion"] = new SelectList(_context.Habitacion, "IdHabitacion", "IdHabitacion", fotoHabitacion.IdHabitacion);
            return View(fotoHabitacion);
        }

        // POST: FotoHabitaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdFotoHabitacion,Foto,IdHabitacion")] FotoHabitacion fotoHabitacion)
        {
            if (id != fotoHabitacion.IdFotoHabitacion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fotoHabitacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FotoHabitacionExists(fotoHabitacion.IdFotoHabitacion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdHabitacion"] = new SelectList(_context.Habitacion, "IdHabitacion", "IdHabitacion", fotoHabitacion.IdHabitacion);
            return View(fotoHabitacion);
        }

        // GET: FotoHabitaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fotoHabitacion = await _context.FotoHabitacion
                .Include(f => f.IdHabitacionNavigation)
                .FirstOrDefaultAsync(m => m.IdFotoHabitacion == id);
            if (fotoHabitacion == null)
            {
                return NotFound();
            }

            return View(fotoHabitacion);
        }

        // POST: FotoHabitaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fotoHabitacion = await _context.FotoHabitacion.FindAsync(id);
            _context.FotoHabitacion.Remove(fotoHabitacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FotoHabitacionExists(int id)
        {
            return _context.FotoHabitacion.Any(e => e.IdFotoHabitacion == id);
        }
    }
}
