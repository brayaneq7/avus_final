﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Avus_02.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;

namespace Avus_02.Controllers
{
    public class HabitacionesController : Controller
    {
        private readonly AvusContext _context;
        private readonly IHostingEnvironment hostingEnvironment;

        public HabitacionesController(AvusContext context, IHostingEnvironment environment)
        {
            _context = context;
            hostingEnvironment = environment;
        }

        // GET: Habitaciones
        public async Task<IActionResult> Index()
        {
            var avusContext = _context.Habitacion.Include(h => h.IdAbueloNavigation);
            return View(await avusContext.ToListAsync());
        }

        // GET: Habitaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var habitacion = await _context.Habitacion
                .Include(h => h.IdAbueloNavigation)
                .FirstOrDefaultAsync(m => m.IdHabitacion == id);
            if (habitacion == null)
            {
                return NotFound();
            }

            return View(habitacion);
        }

        // GET: Habitaciones/Create
        public IActionResult Create()
        {
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo");
            return View();
        }

        // POST: Habitaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdHabitacion,IdAbuelo")] Habitacion habitacion, string Descripcion, string Fumar, string Mascotas, string Invitados, string Wifi, SubirFoto foto)
        {
            //No se puede poner dentro del Bind los valores de los checkboxes porque esto sdevuelven un string 'on' o null.
            if (ModelState.IsValid)
            {
                int intFumar = (Fumar == "on") ? 1 : 0;
                int intMascotas = (Mascotas == "on") ? 1 : 0;
                int intWifi = (Wifi == "on") ? 1 : 0;
                int intInvitados = (Invitados == "on") ? 1 : 0;
                habitacion.Fumar = intFumar;
                habitacion.Mascotas = intMascotas;
                habitacion.Wifi = intWifi;
                habitacion.Invitados = intInvitados;
                habitacion.Disponible = 1;
                habitacion.Descripcion = Descripcion;

                var idAbuelo = HttpContext.Session.GetInt32("id");
                habitacion.IdAbuelo = idAbuelo;
                _context.Add(habitacion);
                await _context.SaveChangesAsync();
                SubirImagenesController.CreateImageRoom(foto, hostingEnvironment, habitacion.IdAbuelo, habitacion.IdHabitacion);
                return RedirectToAction("Habitaciones", "Abuelos");
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", habitacion.IdAbuelo);
            return View(habitacion);
        }

        // GET: Habitaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var habitacion = await _context.Habitacion.FindAsync(id);
            if (habitacion == null)
            {
                return NotFound();
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", habitacion.IdAbuelo);
            return View(habitacion);
        }

        // POST: Habitaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdHabitacion,Descripcion,Fumar,Wifi,Mascotas,Invitados,Disponible,IdAbuelo")] Habitacion habitacion)
        {
            if (id != habitacion.IdHabitacion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(habitacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HabitacionExists(habitacion.IdHabitacion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAbuelo"] = new SelectList(_context.Abuelo, "IdAbuelo", "IdAbuelo", habitacion.IdAbuelo);
            return View(habitacion);
        }

        // GET: Habitaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var habitacion = await _context.Habitacion
                .Include(h => h.IdAbueloNavigation)
                .FirstOrDefaultAsync(m => m.IdHabitacion == id);
            if (habitacion == null)
            {
                return NotFound();
            }

            return View(habitacion);
        }

        // POST: Habitaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var habitacion = await _context.Habitacion.FindAsync(id);
            _context.Habitacion.Remove(habitacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HabitacionExists(int id)
        {
            return _context.Habitacion.Any(e => e.IdHabitacion == id);
        }

        public IActionResult CrearHabitacion()
        {
            return View();
        }

    }
}
