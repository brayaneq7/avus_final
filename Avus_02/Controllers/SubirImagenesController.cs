﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Avus_02.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Avus_02.Controllers
{
    public class SubirImagenesController : Controller
    {

        public SubirImagenesController()
        {
        }
        public IActionResult Index()
        {
            return View();
        }

        // Referencia = 1- Abuelo 2- Estuiante 3-Habitacion
        public static void Create2(SubirFoto model, IHostingEnvironment serverPath, int? id, int referencia)
        {
            createFolder(serverPath);

            if (model.MyImage != null)
            {
                if (ExtensionValidator(model.MyImage.FileName))
                {
                    if (EsHabitacion(referencia))
                    {
                        string folderHabitaciones = Convert.ToString(id); //Poner IDABUELO
                        var namePhoto = SetNamePhoto(model.MyImage.FileName);

                        var uploads = Path.Combine(serverPath.WebRootPath, "images/habitacion");
                        var newFolder = Path.Combine(uploads, folderHabitaciones);
                        Directory.CreateDirectory(newFolder);

                        var filePath = Path.Combine(newFolder, namePhoto);

                        model.MyImage.CopyTo(new FileStream(filePath, FileMode.Create));
                    }
                    else
                    {
                        var namePhoto = SetNamePhotoPerfil(model.MyImage.FileName, id, referencia);

                        var folder = Path.Combine(serverPath.WebRootPath, "images/perfil");

                        var filePath = Path.Combine(folder, namePhoto);

                        model.MyImage.CopyTo(new FileStream(filePath, FileMode.Create));
                    }
                }

            }

            //to do  : Return something
            // return RedirectToAction("Index", "Home");
        }

        public static void CreateImageRoom(SubirFoto model, IHostingEnvironment serverPath, int? id, int IdHabitacion)
        {
            createFolder(serverPath);

            if (model.MyImage != null)
            {
                if (ExtensionValidator(model.MyImage.FileName))
                {
                    var uploads = Path.Combine(serverPath.WebRootPath, "images/habitacion");
                    var filePath = Path.Combine(uploads, Convert.ToString(IdHabitacion)+".jpg");

                    model.MyImage.CopyTo(new FileStream(filePath, FileMode.Create));
                }

            }
            //to do  : Return something
            // return RedirectToAction("Index", "Home");
        }

        private static string SetNamePhoto(string fileName)
        {
            string photoName = Guid.NewGuid().ToString().Substring(0, 4) + Path.GetExtension(fileName); //Nombre Random para photo
            //~~ENVIAR NAME A BBDD~~~
            return photoName;
        }
        private static string SetNamePhotoPerfil(string fileName, int? id, int referencia)
        {
            // A= ABUELO E=ESTUDIANTE
            string photoName = (referencia == 1) ? "A" : "E";
            photoName += Convert.ToString(id) + Path.GetExtension(fileName); //GET ID USER

            //~~ENVIAR NAME A BBDD~~~
            return photoName;
        }

        private static bool ExtensionValidator(string fileName)
        {
            return ".png|.jpg|.gif".Contains(Path.GetExtension(fileName)) ? true : false;
        }

        private static bool EsHabitacion(int referencia)
        {
            return (referencia == 3) ? true : false;
        }
        private static void createFolder(IHostingEnvironment serverPath)
        {
            var uploads1 = Path.Combine(serverPath.WebRootPath, "images/perfil");
            var uploads2 = Path.Combine(serverPath.WebRootPath, "images/habitacion");
            Directory.CreateDirectory(uploads1);
            Directory.CreateDirectory(uploads2);
        }
    }
}