﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Avus_02.Models;
using Avus_02.Model;
using Microsoft.AspNetCore.Http;

namespace Avus_02.Controllers
{
    public class LoginController : Controller
    {
        private readonly AvusContext _context;

        public LoginController(AvusContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var tipo = HttpContext.Session.GetString("tipo");
            if (tipo != null) { 
            if (tipo.Equals("abuelo"))
            {
                return RedirectToAction("Solicitudes", "Abuelos");
            }
            if (tipo.Equals("estudiante"))
            {
                return RedirectToAction("Solicitudes", "Estudiantes");
            } }

            return View();
        }



        public IActionResult EstudianteYAbuelo()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
        
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }
        public IActionResult About()
        {
            return View();
        }
        // POST: Abuelos/Search(email)
        [HttpPost]
        public IActionResult SearchEmail(string email, string password)
        {
            if (email == null || password == null)
            {
                return NotFound();
            }
            //Si lo queremos realizar de una manera asyncorna tendriamos que recojer toda la bbdd y crear un bucle, el where no es async.
            //Where buscando por email y password 
            // utilizamos el metodo computesha para encryptarlo en hash y compararlo con nuestra clave hash en nuestra bbdd 
            var abuelo = _context.Abuelo.Where(predicate: s => s.Email.Equals(email) && s.Password.Equals(Encriptar.ComputeSha256Hash(password))).FirstOrDefault();
            var estudiante = _context.Estudiante.Where(predicate: s => s.Email.Equals(email) && s.Password.Equals(Encriptar.ComputeSha256Hash(password))).FirstOrDefault();
            
            if (abuelo != null)
            {
                //Cookie session donde guardamos solo el ID 
                HttpContext.Session.SetInt32("id",abuelo.IdAbuelo);
                HttpContext.Session.SetString("tipo", "abuelo");


                //Esto redirige al controlador Abuelos, método Solicitudes para que la url sea correcta.
                return RedirectToAction("Solicitudes", "Abuelos");

            }
            else if (estudiante != null)
            {
                HttpContext.Session.SetInt32("id", estudiante.IdEstudiante);
                HttpContext.Session.SetString("tipo", "estudiante");
                return RedirectToAction("Busqueda", "Estudiantes");
            }
            else {
                return NotFound();
            }

            
        }
    }
}
