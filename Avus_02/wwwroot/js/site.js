﻿
//Used to make the plus card same size as a room card everytime no matter the width of the window.

//Utilidad para mejoras, como no tener que recargar la pagina o realizar menos llamadas al servidor.
var listaPeticionesAceptadas = [];
var listaEstudiantesAceptados = [];
var listaPeticionesProceso = [];
var listaEstudiantesProceso = [];
var listaPeticionesRecibidas = [];
var listaEstudiantesRecibidas = [];
var listaDeAbuelos = [];
var listaDeHabitaciones = [];

function iguala() {

    let height = $('#cartaLlena').css('height');
    $('#cartaVacia').css('height', height);
}

window.addEventListener('resize', iguala);

$(document).ready(function () {
    iguala();
});


//Used to make a new room card

//Card where everything will go
function crearCarta(index, disponible,descripcion, invitados, fumar, mascotas, wifi, foto) {
    let empty = $('#emptyDiv');

    let column = $('<div>').addClass('col-md-6');
    let card = $('<div>').addClass('card m-3').attr('id', 'cartaLlena').css('max-width','520px');
    let row1 = $('<div>').addClass('row');
    let col1 = $('<div>').addClass('col-xl-6 text-center');
    let divimage = $('<div>').addClass('d-flex align-items-center justify-content-center m-3 mt-xl-2 ml-xl-2 mb-xl-2 mr-xl-n3');
    let image = $('<img>').addClass('card-img rounded').attr('alt', '...').attr('src', foto);
    let divtitle = $('<div>').addClass('text-center mt-2');
    let title = $('<h1>').addClass('card-title responsive-title').text('Habitación '+index);
    let col2 = $('<div>').addClass('col-xl-6');
    let cardbody = $('<div>').addClass('card-body mr-2');
    let row2 = $('<div>').addClass('row');
    let divbodytext = $('<div>').addClass('card-text mt-n2 mb-2 w-100');
    let bodytext = $('<textarea>').addClass('card-text m-1 m-xl-0 text-justify boxsizingBorder').text(descripcion).attr('readonly', '').css('min-height', '120px');

    let row3 = $('<div>').addClass('row');
    //There are 5 checkboxes with different id and different values that determine the status of the checkbox (checked or unchecked)
    let invitadosbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
    let invitadosdivcheckbox = $('<div>').addClass('form-check form-check-inline');
    let invitadoscheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'invitados').attr('checked', invitados == 1 ? true: false).attr('disabled', true);
    let invitadoslabel = $('<label>').addClass('form-check-label').attr('for', 'invitados').text('Invitados');

    let fumadoresbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
    let fumadoresdivcheckbox = $('<div>').addClass('form-check form-check-inline');
    let fumadorescheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'fumadores').attr('checked', fumar == 1 ? true : false).attr('disabled', true);
    let fumadoreslabel = $('<label>').addClass('form-check-label').attr('for', 'fumadores').text('Fumadores');

    let mascotasbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
    let mascotasdivcheckbox = $('<div>').addClass('form-check form-check-inline');
    let mascotascheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'mascotas').attr('checked', mascotas == 1 ? true : false).attr('disabled', true);
    let mascotaslabel = $('<label>').addClass('form-check-label').attr('for', 'mascotas').text('Mascotas');

    let wifibigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
    let wifidivcheckbox = $('<div>').addClass('form-check form-check-inline');
    let wificheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'wifi').attr('checked', wifi == 1 ? true : false).attr('disabled', true);
    let wifilabel = $('<label>').addClass('form-check-label').attr('for', 'wifi').text('Wi-Fi');

    let row4 = $('<div>').addClass('row');
    let disponiblebigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
    let disponibledivcheckbox = $('<div>').addClass('form-check form-check-inline');
    let disponiblecheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'disponible').attr('checked', disponible == 1 ? true : false).attr('disabled', true);
    let disponiblelabel = $('<label>').addClass('form-check-label').attr('for', 'disponible').text('Disponible');

    let diveditbutton = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left d-inline-block');
    let editbutton = $('<a>').addClass('btn btn-primary btn-sm mb-2').attr('href', '#').attr('role', 'button').text('Editar');

    let newemptydiv = $('<div>').addClass('col-md-6 d-none').attr('id', 'emptyDiv');

    let columna1 = col1.append(
        divimage.append(image),
        divtitle.append(title));

    let columna2 = col2.append(cardbody.append(

        row2.append(divbodytext.append(bodytext)),
        row3.append(
            invitadosbigdivcheckbox.append(invitadosdivcheckbox.append(invitadoscheckbox, invitadoslabel)),
            fumadoresbigdivcheckbox.append(fumadoresdivcheckbox.append(fumadorescheckbox, fumadoreslabel)),
            mascotasbigdivcheckbox.append(mascotasdivcheckbox.append(mascotascheckbox, mascotaslabel)),
            wifibigdivcheckbox.append(wifidivcheckbox.append(wificheckbox, wifilabel))),
        row4.append(
            disponiblebigdivcheckbox.append(disponibledivcheckbox.append(disponiblecheckbox, disponiblelabel)),
            diveditbutton.append(editbutton)))
    );
    empty.removeClass('d-none');
    empty.removeAttr('id');
    empty.append(card.append((row1.append(columna1, columna2))));
    empty.after(newemptydiv);

}


function ordenarSolicitudes(listasolicitudes, listaestudiantes) {
    //recorremos el Array de solicitudes comparandola con los estudiantes
    for (var solicitudes in listasolicitudes) {
        if (listasolicitudes[solicitudes].fecha_primer_dia != "" && listasolicitudes[solicitudes].fecha_primer_dia != null) {
            listaPeticionesAceptadas.push(listasolicitudes[solicitudes]);
            for (var estudiante in listaestudiantes) {
                if (listaestudiantes[estudiante].id == listasolicitudes[solicitudes].id_estudiante) {
                    listaEstudiantesAceptados.push(listaestudiantes[estudiante]);
                    crearCartaEstudiante(listaestudiantes[estudiante], "final", listasolicitudes[solicitudes].id, listasolicitudes[solicitudes].nombre);
                }
            }
        } else if (listasolicitudes[solicitudes].aceptacion > 0) {
            listaPeticionesProceso.push(solicitudes);
            for (var estudiante in listaestudiantes) {
                if (listaestudiantes[estudiante].id == listasolicitudes[solicitudes].id_estudiante) {
                    listaEstudiantesAceptados.push(listaestudiantes[estudiante]);
                    crearCartaEstudiante(listaestudiantes[estudiante], "aceptarsegunda", listasolicitudes[solicitudes].id, listasolicitudes[solicitudes].nombre);
                }
            }
        } else {
            listaPeticionesRecibidas.push(solicitudes);
            for (var estudiante in listaestudiantes) {
                if (listaestudiantes[estudiante].id == listasolicitudes[solicitudes].id_estudiante) {
                    listaEstudiantesRecibidas.push(listaestudiantes[estudiante]);
                    crearCartaEstudiante(listaestudiantes[estudiante], "aceptar", listasolicitudes[solicitudes].id, listasolicitudes[solicitudes].nombre);
                }
            }
        }
    }
}
function crearCartaEstudiante(estudiante, tipo, idsolicitud,nombreEstudiante) {
    //variable que selecciona el div correspondiente al tipo de solicitud que sea
    let peticionesid;
    //creador de carta
    let contenedorcarta = $('<div></div>').addClass('col-12 col-sm-12 col-md-6 align-items-stretch');
    let carta = $("<div></div>").addClass("card bg-light-tr");
    let cartabody = $("<div></div>").addClass("card-body");
    let cartarow = $("<div></div>").addClass("row");
    let rowfooter = $("<div></div>").addClass("row");
    let cartacolumfoto = $("<div></div>").addClass("col-12 text-center");
    let imagenperfil = $("<img></img>").addClass("responsive").attr("src", "/Abuelos/Image/" + estudiante.id);//poner estudiante.fot
    cartacolumfoto.append(imagenperfil);
    let cartarownombre = $("<div></div>").addClass("row");
    let cartacolumnombre = $("<div></div>").addClass("col-12 text-center");
    let nombre = $("<h3></h3>").text(nombreEstudiante);
    cartacolumnombre.append(nombre);
    let cartafooter = $("<div></div>").addClass("card-footer");

    let contenedorbotones = $("<div></div>").addClass("col-6 col-md-12 col-xl-8 text-left text-md-center text-lg-left mb-md-3 mb-lg-0 order-md-last order-lg-first");
    let botonborrar = $("<button></button>").addClass("btn btn-danger").text("Borrar solicitud").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Abuelos/BorrarSolicitud/' + idsolicitud; });

    let contenedorbotonperfil = $("<div></div>").addClass("col-6 col-md-12 col-xl-4 text-right text-md-center text-lg-right mb-md-3 mb-lg-0");
    let aboton = $("<a></a>").addClass("btn btn-primary text-light").attr("id", "perfil").attr("role", "button").click(function () { location.href = '/Abuelos/PerfilEstudiante/' + estudiante.id; });
    let iboton = $("<i></i>").addClass("fa fa-user").text("Ver Perfil");

    contenedorcarta.append(carta);
    carta.append(cartabody);
    cartabody.append(cartarow);
    carta.append(cartarownombre);
    carta.append(cartafooter);
    cartarow.append(cartacolumfoto);
    cartarownombre.append(cartacolumnombre);
    cartafooter.append(rowfooter);
    //dependiendo de que tipo de solicitud es creamos un tipo de boton
    if (tipo == "aceptar") {
        let botonaceptar = $("<button></button>").addClass("btn btn-success").text("Aceptar Solicitud").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Abuelos/cambiarSolicitudAceptar/' + idsolicitud; });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesRecibidas');
    } else if (tipo == "aceptarsegunda") {
        let botonaceptar = $("<button></button>").addClass("btn btn-success").text("Aceptar Solicitud").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Abuelos/cambiarSolicitudProceso/' + idsolicitud; });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesProceso');
    }  else if(tipo == "final") {
        let botonaceptar = $("<button></button>").addClass("btn btn-success").text("Terminar Periodo").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Abuelos/cambiarSolicitud/' + idsolicitud });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesAceptadas');
    } 
    contenedorbotones.append(botonborrar);
    rowfooter.append(contenedorbotonperfil);
    contenedorbotonperfil.append(aboton);
    aboton.append(iboton);

    peticionesid.append(contenedorcarta);

}

function crearEstudiante(estudiante, opiniones) {
    var countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    var sexo = [
        "Hombre", "Mujer", "Otros"
    ];
    var estudios = [
        "Ciclo formativo grado superior", "Graduado universitario", "Masters y postgrados", "Otros"
    ];
    $('#nombrecompleto').text(estudiante.nombre + " " + estudiante.apellidos);
    $('#email').text(estudiante.email);
    $('#edad').text(estudiante.edad);
    $('#telefono').text(estudiante.telefono);
    $('#tab_default_1').text(estudiante.descripcion);
    $('#estudios').text(estudios[estudiante.estudios - 1]);
    $('#nacionalidad').text(countries[estudiante.nacionalidad]);
    $('#genero').text(sexo[estudiante.sexo - 1]);
    $('#imagenPerfilestudiante').attr("src", "/Abuelos/Image/" + estudiante.id);
    var listaopiniones = $('#opiniones');
    for (var posicionopinion in opiniones) {
        var columna = $('<li></li>').addClass('list-group-item').text(opiniones[posicionopinion].contenido);
        listaopiniones.append(columna);

    }
    //Añadir opiniones
    let botonAceptar = $("<button></button>").addClass("btn btn-primary").text("Añadir Opinión").attr("type", "submit").attr("id", "addtoserver").click(function () {

    });;
    let form = $('<form>').attr('action', '../AddOpinion'); //method="post"
    let formdiv = $('<div>').addClass('form-group');
    let formlaber = $('<label></label>').text('Añadir Opinión');
    let formtextarea = $('<textarea></textarea>').attr('name', 'Opinion').addClass('form-control');
    let formidoculto = $('<input></input>').attr('name', 'IdEstudiante').val(estudiante.id).hide();


    formdiv.append(formlaber);
    formdiv.append(formtextarea);
    formdiv.append(formidoculto);
    form.append(formdiv);
    form.append(botonAceptar);
    let botonAddOpinion = $("<button></button>").addClass("btn btn-primary").text("Añadir Opinión").attr("type", "submit").attr("id", "addopinion").click(function () {
        //Este boton nos mostrara , añadir opiniones
        $('#tab_default_2').append(form);
        botonAddOpinion.hide();
    });
    $('#tab_default_2').append(botonAddOpinion);
}

function visualizarAbuelos(abuelosjs, habitacionesjs) {

    for (var posicion in abuelosjs) {

        let abuelo = abuelosjs[posicion];
        listaDeAbuelos.push(abuelo);
        //variable que selecciona el div correspondiente al tipo de solicitud que sea
        let peticionesid = $("#peitcionesAceptadas");
        //creador de carta

        let carta = $("<div></div>").addClass("card");
        
        let imagenperfil = $("<img></img>").addClass("responsive").attr("src", "/Estudiantes/ImageAbuelo/" + abuelo.id);//poner estudiante.fot
 
        let nombre = $("<h1></h1>").text(abuelo.nombre + " " + abuelo.apellidos);

        let descripcion = $("<p></p>").text(abuelo.descripcion);
 

        let perfil = $("<button></button>").text("Perfil").attr("type", "submit").click(function () { location.href = '/Estudiantes/PerfilAbuelo/' + abuelo.id; });

        carta.append(imagenperfil);
        carta.append(nombre);
        carta.append(descripcion);
        carta.append(perfil);

        peticionesid.append(carta);

    }
    for (var posicion in habitacionesjs) {
        let habitacion = habitacionesjs[posicion];
        listaDeHabitaciones.push(habitacion);
    }
    
/* <div class="card">
<img src="~/images/perfil/A8009.jpg" alt="John" style="width:100%">
<h1>John Doe</h1>
<p class="title">CEO & Founder, Example</p>
<p>Harvard University</p>
<div style="margin: 24px 0;">
 <a href="#"><i class="fa fa-dribbble"></i></a>
 <a href="#"><i class="fa fa-twitter"></i></a>
 <a href="#"><i class="fa fa-linkedin"></i></a>
 <a href="#"><i class="fa fa-facebook"></i></a>
</div>
<p><button>Perfil</button></p>
         </div>*/

}
function perfildelabuelo(abuelosjs, habitacionesjs, opinionesjs) {

    let abuelo = abuelosjs[0];

    var countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    var sexo = [
        "Hombre", "Mujer", "Otros"
    ];

    $('#nombrecompleto').text(abuelo.nombre + " " + abuelo.apellidos);
    //$('#email').text(abuelo.email);
    $('#edad').text(abuelo.edad);
    //$('#telefono').text(abuelo.telefono);
    $('#tab_default_1').text(abuelo.descripcion);
    $('#telefono').text(abuelo.telefono);
    $('#email').text(abuelo.email);
    //$('#estudios').text(estudios[abuelo.estudios - 1]);
    $('#nacionalidad').text(countries[abuelo.nacionalidad]);
    $('#genero').text(sexo[abuelo.sexo - 1]);
    $('#imagenPerfilestudiante').attr("src", "/Estudiantes/ImageAbuelo/" + abuelo.id);

    var listaopiniones = $('#opiniones');
    for (var posicionopinion in opinionesjs) {
        var columna = $('<li></li>').addClass('list-group-item').text(opinionesjs[posicionopinion].contenido);
        listaopiniones.append(columna);
    }
    let contador = 1;
    //Habitaciones
    for (var posicion in habitacionesjs) {
        let disp = false;
        let masc = false;
        let invi = false;
        let fuma = false;
        let wi = false;
        
        let habitacion = habitacionesjs[posicion];
        if (habitacion.disponible == 1) {
            disp = true;
        }

        if (habitacion.mascotas == 1) {
            masc = true;
        }
        if (habitacion.invitados == 1) {
            invi = true;
        }
        if (habitacion.fumadores == 1) {
            fuma = true;
        }
        if (habitacion.wifi == 1) {
            wi = true;
        }
        let card = $('<div>').addClass('card m-3').css('max-width', '520px');
        let row1 = $('<div>').addClass('row');
        let col1 = $('<div>').addClass('col-xl-6 text-center');
        let divimage = $('<div>').addClass('d-flex align-items-center justify-content-center m-3 mt-xl-2 ml-xl-2 mb-xl-2 mr-xl-n3');
        let image = $('<img>').addClass('card-img rounded').attr('alt', '...').attr('src', "/Estudiantes/ImageHabitacion/" + habitacion.id);
        let divtitle = $('<div>').addClass('text-center mt-2');
        let title = $('<h1>').addClass('card-title responsive-title').text('Habitación ' + (contador));
        let col2 = $('<div>').addClass('col-xl-6');
        let cardbody = $('<div>').addClass('card-body mr-2');
        let row2 = $('<div>').addClass('row');
        let divbodytext = $('<div>').addClass('card-text mb-2');
        let bodytext = $('<textarea >').addClass('card-text m-1 m-xl-0 text-justify form-control').text(habitacion.descripcion).attr('readonly', '').css('min-height', '120px');

        let row3 = $('<div>').addClass('row');
        //There are 5 checkboxes with different id and different values that determine the status of the checkbox (checked or unchecked)
        let invitadosbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let invitadosdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let invitadoscheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'invitados').attr('checked', invi).attr('disabled', true);
        let invitadoslabel = $('<label>').addClass('form-check-label').attr('for', 'invitados').text('Invitados');

        let fumadoresbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let fumadoresdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let fumadorescheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'fumadores').attr('checked', fuma).attr('disabled', true);
        let fumadoreslabel = $('<label>').addClass('form-check-label').attr('for', 'fumadores').text('Fumadores');

        let mascotasbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let mascotasdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let mascotascheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'mascotas').attr('checked', masc).attr('disabled', true);
        let mascotaslabel = $('<label>').addClass('form-check-label').attr('for', 'mascotas').text('Mascotas');

        let wifibigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let wifidivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let wificheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'wifi').attr('checked', wi).attr('disabled', true);
        let wifilabel = $('<label>').addClass('form-check-label').attr('for', 'wifi').text('Wi-Fi');

        let row4 = $('<div>').addClass('row');
        let disponiblebigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let disponibledivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let disponiblecheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'disponible').attr('checked', disp).attr('disabled', true);
        let disponiblelabel = $('<label>').addClass('form-check-label').attr('for', 'disponible').text('Disponible');

        let diveditbutton = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left d-inline-block');
        let editbutton = $('<a>').addClass('btn btn-primary btn-sm mb-2').attr('href', '../CrearSolicitud/' + habitacion.id).attr('role', 'button').text('Enviar Solicitud');

        let newemptydiv = $('<div>').addClass('col-md-6 d-none').attr('id', 'emptyDiv');

        let columna1 = col1.append(
            divimage.append(image),
            divtitle.append(title));

        let columna2 = col2.append(cardbody.append(

            row2.append(divbodytext.append(bodytext)),
            row3.append(
                invitadosbigdivcheckbox.append(invitadosdivcheckbox.append(invitadoscheckbox, invitadoslabel)),
                fumadoresbigdivcheckbox.append(fumadoresdivcheckbox.append(fumadorescheckbox, fumadoreslabel)),
                mascotasbigdivcheckbox.append(mascotasdivcheckbox.append(mascotascheckbox, mascotaslabel)),
                wifibigdivcheckbox.append(wifidivcheckbox.append(wificheckbox, wifilabel))),
            row4.append(
                disponiblebigdivcheckbox.append(disponibledivcheckbox.append(disponiblecheckbox, disponiblelabel)),
                diveditbutton.append(editbutton)))
        );

        row1.append(columna1);
        row1.append(columna2);
        card.append(row1);
        $('#tab_default_3').append(card);
        contador++;

    }
    


}
function ordenarSolicitudes2(listasolicitudes, listahabitacion) {
    //recorremos el Array de solicitudes comparandola con los estudiantes
    for (var solicitudes in listasolicitudes) {
        if (listasolicitudes[solicitudes].fecha_primer_dia != "" && listasolicitudes[solicitudes].fecha_primer_dia != null) {
            for (var habitacion in listahabitacion) {
                if (listahabitacion[habitacion].id == listasolicitudes[solicitudes].id_habitacion) {
                    //console.log("Estamos en final -> "++);
                    crearCartaHabitacion(listahabitacion[habitacion], "final2", listasolicitudes[solicitudes].id);
                }
            }
        } else if (listasolicitudes[solicitudes].aceptacion > 0) {
            for (var habitacion in listahabitacion) {
                if (listahabitacion[habitacion].id == listasolicitudes[solicitudes].id_habitacion) {
                    
                    crearCartaHabitacion(listahabitacion[habitacion], "aceptarsegunda2", listasolicitudes[solicitudes].id);
                }
            }
        } else {
            for (var habitacion in listahabitacion) {
                if (listahabitacion[habitacion].id == listasolicitudes[solicitudes].id_habitacion) {
                    
                    crearCartaHabitacion(listahabitacion[habitacion], "aceptar2", listasolicitudes[solicitudes].id);
                }
            }
        }
    }
}
function crearCartaHabitacion(habitacion, tipo, idsolicitud) {
    //variable que selecciona el div correspondiente al tipo de solicitud que sea
    let peticionesid;
    //creador de carta
    let contenedorcarta = $('<div></div>').addClass('col-12 col-sm-12 col-md-6 align-items-stretch');
    let carta = $("<div></div>").addClass("card bg-light-tr");
    let cartabody = $("<div></div>").addClass("card-body");
    let cartarow = $("<div></div>").addClass("row");
    let rowfooter = $("<div></div>").addClass("row");
    let cartacolumfoto = $("<div></div>").addClass("col-12 text-center");
    let imagenperfil = $("<img></img>").addClass("responsive").attr("src", "/Estudiantes/ImageHabitacion/" + habitacion.id);//poner habitacion.fot
    cartacolumfoto.append(imagenperfil);
    let cartarownombre = $("<div></div>").addClass("row");
    let cartacolumnombre = $("<div></div>").addClass("col-12 text-center");
    let nombre = $("<h3></h3>").text(habitacion.name);
    cartacolumnombre.append(nombre);
    let cartafooter = $("<div></div>").addClass("card-footer");

    let contenedorbotones = $("<div></div>").addClass("col-6 col-md-12 col-xl-8 text-left text-md-center text-lg-left mb-md-3 mb-lg-0 order-md-last order-lg-first");

    let contenedorbotonperfil = $("<div></div>").addClass("col-6 col-md-12 col-xl-4 text-right text-md-center text-lg-right mb-md-3 mb-lg-0");
    let aboton = $("<a></a>").addClass("btn btn-primary text-light").attr("id", "perfil").attr("role", "button").click(function () { location.href = '/Estudiantes/PerfilAbuelo/' + habitacion.id_abuelo; });
    let iboton = $("<i></i>").addClass("fa fa-user").text("Ver Perfil");

    contenedorcarta.append(carta);
    carta.append(cartabody);
    cartabody.append(cartarow);
    carta.append(cartarownombre);
    carta.append(cartafooter);
    cartarow.append(cartacolumfoto);
    cartarownombre.append(cartacolumnombre);
    cartafooter.append(rowfooter);
    //dependiendo de que tipo de solicitud es creamos un tipo de boton
    if (tipo == "aceptar2") {
        let botonaceptar = $("<button></button>").addClass("btn btn-danger").text("Eliminar Solicitud").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Estudiantes/BorrarSolicitud/' + idsolicitud; });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesRecibidas');
    } else if (tipo == "aceptarsegunda2") {
        let botonaceptar = $("<button></button>").addClass("btn btn-danger").text("Eliminar Solicitud").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Estudiantes/BorrarSolicitud/' + idsolicitud; });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesProceso');
    } else {
        let botonaceptar = $("<button></button>").addClass("btn btn-danger").text("Terminar Periodo").attr("type", "submit").attr("id", idsolicitud).click(function () { location.href = '/Estudiantes/cambiarSolicitud/' + idsolicitud });
        contenedorbotones.append(botonaceptar);
        rowfooter.append(contenedorbotones);
        peticionesid = $('#peitcionesAceptadas');
    }
    rowfooter.append(contenedorbotonperfil);
    contenedorbotonperfil.append(aboton);
    aboton.append(iboton);

    peticionesid.append(contenedorcarta);

}

function crearEstudianteMio(estudiante, opiniones) {
    var countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    var sexo = [
        "Hombre", "Mujer", "Otros"
    ];
    var estudios = [
        "Ciclo formativo grado superior", "Graduado universitario", "Masters y postgrados", "Otros"
    ];
    $('#nombrecompleto').text(estudiante.nombre + " " + estudiante.apellidos);
    $('#email').text(estudiante.email);
    $('#edad').text(estudiante.edad);
    $('#telefono').text(estudiante.telefono);
    $('#tab_default_1').text(estudiante.descripcion);
    $('#estudios').text(estudios[estudiante.estudios - 1]);
    $('#nacionalidad').text(countries[estudiante.nacionalidad]);
    $('#genero').text(sexo[estudiante.sexo - 1]);
    $('#imagenPerfilestudiante').attr("src", "/Abuelos/Image/" + estudiante.id);
    var listaopiniones = $('#opiniones');
    for (var posicionopinion in opiniones) {
        var columna = $('<li></li>').addClass('list-group-item').text(opiniones[posicionopinion].contenido);
        listaopiniones.append(columna);

    }   
}
function perfildelabueloMio(abuelosjs, habitacionesjs, opinionesjs) {

    let abuelo = abuelosjs[0];

    var countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    var sexo = [
        "Hombre", "Mujer", "Otros"
    ];

    $('#nombrecompleto').text(abuelo.nombre + " " + abuelo.apellidos);
    //$('#email').text(abuelo.email);
    $('#edad').text(abuelo.edad);
    //$('#telefono').text(abuelo.telefono);
    $('#tab_default_1').text(abuelo.descripcion);
    //$('#estudios').text(estudios[abuelo.estudios - 1]);
    $('#nacionalidad').text(countries[abuelo.nacionalidad]);
    $('#genero').text(sexo[abuelo.sexo - 1]);
    $('#imagenPerfilestudiante').attr("src", "/Estudiantes/ImageAbuelo/" + abuelo.id);

    var listaopiniones = $('#opiniones');
    for (var posicionopinion in opinionesjs) {
        var columna = $('<li></li>').addClass('list-group-item').text(opinionesjs[posicionopinion].contenido);
        listaopiniones.append(columna);
    }

    //Habitaciones
    for (var posicion in habitacionesjs) {
        let disp = false;
        let masc = false;
        let invi = false;
        let fuma = false;
        let wi = false;
        let habitacion = habitacionesjs[posicion];
        if (habitacion.disponible == 1) {
            disp = true;
        }

        if (habitacion.mascotas == 1) {
            masc = true;
        }
        if (habitacion.invitados == 1) {
            invi = true;
        }
        if (habitacion.fumadores == 1) {
            fuma = true;
        }
        if (habitacion.wifi == 1) {
            wi = true;
        }
        let card = $('<div>').addClass('card m-3').css('max-width', '520px');
        let row1 = $('<div>').addClass('row');
        let col1 = $('<div>').addClass('col-xl-6 text-center');
        let divimage = $('<div>').addClass('d-flex align-items-center justify-content-center m-3 mt-xl-2 ml-xl-2 mb-xl-2 mr-xl-n3');
        let image = $('<img>').addClass('card-img rounded').attr('alt', '...').attr('src', "/Estudiantes/ImageHabitacion/" + habitacion.id);
        let divtitle = $('<div>').addClass('text-center mt-2');
        let title = $('<h1>').addClass('card-title responsive-title').text('Habitación ' + posicion);
        let col2 = $('<div>').addClass('col-xl-6');
        let cardbody = $('<div>').addClass('card-body mr-2');
        let row2 = $('<div>').addClass('row');
        let divbodytext = $('<div>').addClass('card-text mb-2');
        let bodytext = $('<textarea >').addClass('card-text m-1 m-xl-0 text-justify form-control').text(habitacion.descripcion).attr('readonly', '').css('min-height', '120px');

        let row3 = $('<div>').addClass('row');
        //There are 5 checkboxes with different id and different values that determine the status of the checkbox (checked or unchecked)
        let invitadosbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let invitadosdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let invitadoscheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'invitados').attr('checked', invi).attr('disabled', true);
        let invitadoslabel = $('<label>').addClass('form-check-label').attr('for', 'invitados').text('Invitados');

        let fumadoresbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let fumadoresdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let fumadorescheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'fumadores').attr('checked', fuma).attr('disabled', true);
        let fumadoreslabel = $('<label>').addClass('form-check-label').attr('for', 'fumadores').text('Fumadores');

        let mascotasbigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let mascotasdivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let mascotascheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'mascotas').attr('checked', masc).attr('disabled', true);
        let mascotaslabel = $('<label>').addClass('form-check-label').attr('for', 'mascotas').text('Mascotas');

        let wifibigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let wifidivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let wificheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'wifi').attr('checked', wi).attr('disabled', true);
        let wifilabel = $('<label>').addClass('form-check-label').attr('for', 'wifi').text('Wi-Fi');

        let row4 = $('<div>').addClass('row');
        let disponiblebigdivcheckbox = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left');
        let disponibledivcheckbox = $('<div>').addClass('form-check form-check-inline');
        let disponiblecheckbox = $('<input>').addClass('form-check-input active').attr('type', 'checkbox').attr('id', 'disponible').attr('checked', disp).attr('disabled', true);
        let disponiblelabel = $('<label>').addClass('form-check-label').attr('for', 'disponible').text('Disponible');

        let diveditbutton = $('<div>').addClass('col-6 pl-5 ml-md-n3 ml-lg-0 pl-xl-0 text-xl-left d-inline-block');
        let editbutton = $('<a>').addClass('btn btn-primary btn-sm mb-2').attr('href', '../CrearSolicitud/' + habitacion.id).attr('role', 'button').text('Enviar Solicitud');

        let newemptydiv = $('<div>').addClass('col-md-6 d-none').attr('id', 'emptyDiv');

        let columna1 = col1.append(
            divimage.append(image),
            divtitle.append(title));

        let columna2 = col2.append(cardbody.append(

            row2.append(divbodytext.append(bodytext)),
            row3.append(
                invitadosbigdivcheckbox.append(invitadosdivcheckbox.append(invitadoscheckbox, invitadoslabel)),
                fumadoresbigdivcheckbox.append(fumadoresdivcheckbox.append(fumadorescheckbox, fumadoreslabel)),
                mascotasbigdivcheckbox.append(mascotasdivcheckbox.append(mascotascheckbox, mascotaslabel)),
                wifibigdivcheckbox.append(wifidivcheckbox.append(wificheckbox, wifilabel))),
            row4.append(
                disponiblebigdivcheckbox.append(disponibledivcheckbox.append(disponiblecheckbox, disponiblelabel))
               ))
        );

        row1.append(columna1);
        row1.append(columna2);
        card.append(row1);
        $('#tab_default_3').append(card);


    }



}