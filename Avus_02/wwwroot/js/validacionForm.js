//validaciomForm.js

function FormAbuelo() {

    //Esto comprueba en tiempo real los mensajes de invalid o valid
    $("#Nombre").keyup(comprobarName);
    $("#Apellidos").keyup(comprobarSurname);
    $("#Dni").keyup(comprobarDni);
    $("#Email").keyup(comprobarEmail);
    $("#Password").keyup(comprobarContraseña);  
    $("#Telefono").keyup(comprobarTelefono);
    $("#Pais").keyup(comprobarPais); //esto va bien, pero se ha realizado una function onchange para tener mas control
    $("#Comunidad").keyup(comprobarComunidad);
    $("#Localidad").keyup(comprobarLocalidad); 
    $("#Direccion").keyup(comprobarDireccion);
    $("#CodigoPostal").keyup(comprobarCodigoPostal);

    //Metodos change del abuelo
    changeFecha();
    changeSexo();
    changePais();
    changeComunidad();
    changeLocalidad();
    //falta el change de localidad

    //Autocompletado del pais
    autocompletadoPais();

    //Autocompletado de la comunidad del abuelo
    autocompletadoComunidad();

    //Autocompletado de la localidad del abuelo
    autocompletadoLocalidad();

    //Comprobacion al clicar el boton "Enviar"
    $("#btnvalidar").click(function () {

        comprobarName();
        comprobarSurname();
        comprobarDni();
        comprobarEmail();
        comprobarContraseña();
        comprobarTelefono();
        comprobarPais();
        comprobarFecha();
        comprobarSexo();
        comprobarComunidad();
        comprobarLocalidad();
        comprobarCodigoPostal();
        comprobarDireccion();
        if ($(".is-invalid")[0] == null) {

            document.getElementById("abueloForm").submit();
        }
        
    });

}

function FormEstudiante() {

    //Esto comprueba en tiempo real los mensajes de invalid o valid
    $("#Nombre").keyup(comprobarName);
    $("#Apellidos").keyup(comprobarSurname);    
    $("#Dni").keyup(comprobarDni);
    $("#Email").keyup(comprobarEmail);
    $("#Password").keyup(comprobarContraseña);
    $("#Nacionalidad").keyup(comprobarNacionalidad);  
    $("#Telefono").keyup(comprobarTelefono);
    $("#Estudios").keyup(comprobarEstudios);

    //Metodos change del estudiante
    changeFecha();
    changeNacionalidad();
    changeEstudios();
    changeSexo();

    //Autocompletado de la nacionalidad
    autocompletadoNacionalidad();

    //Comprobacion al clicar el boton "Registrar"
    $("#btnvalidar").click(function () {

        comprobarName();
        comprobarSurname();
        comprobarDni();
        comprobarEmail();
        comprobarContraseña();
        comprobarNacionalidad();
        comprobarFecha();
        comprobarTelefono();
        comprobarEstudios();
        comprobarSexo();
        if ($(".is-invalid")[0] == null) {
            
            document.getElementById("estudianteForm").submit();
        }

    });

}

//Autocompletado de la Nacionalidad del estudiante
function autocompletadoNacionalidad() {

    let countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    let nacionalidadTipos = document.getElementById('Nacionalidad');
    autocomplete(nacionalidadTipos, countries);

}

//Autocompletado del pais del abuelo
function autocompletadoPais() {

    let countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];
    let paisTipos = document.getElementById('Pais');
    autocomplete(paisTipos, countries);
}

//Autocompletado de la comunidad del abuelo
function autocompletadoComunidad() {

    let comunidad = [
        "Andalucía", "Aragón", "Asturias", "Canarias", "Cantabria", "Castilla La Mancha", "Castilla y León", "Cataluña", "Comunidad de Madrid", "Comunidad Valenciana",
        "Extremadura", "Galicia", "Islas Baleares", "La Rioja", "Murcia", "Navarra", "País Vasco"];

    let comunidadTipos = document.getElementById('Comunidad');
    autocomplete(comunidadTipos, comunidad);
}

//Autocompletado de la comunidad del abuelo
function autocompletadoLocalidad() {

    let localidadAndalucia = ["Almería", "Cádiz", "Córdoba", "Granada", "Huelva", "Jaén", "Málaga", "Sevilla"];
    let localidadAragon = ["Huesca", "Teruel", "Zaragoza"];   
    let localidadAsturias = ["Asturias"];
    let localidadCanarias = ["Las Palmas", "Santa Cruz de Tenerife"];
    let localidadCantabria = ["Cantabria"];
    let localidadCastillaMancha = ["Albacete", "Ciudad Real", "Cuenca", "Guadalajara", "Toledo"];
    let localidadCastillaLeon = ["Ávila", "Burgos", "León", "Palencia", "Salamanca", "Segovia", "Soria", "Valladolid ", "Zamora"];
    let localidadCataluña = ["Barcelona", "Girona", "Lleida ", "Tarragona"];
    let localidadValenciana = ["Alicante", "Castellón ", "Valencia"];
    let localidadMadrid = ["Madrid"];
    let localidadExtremadura = ["Badajoz ", "Cáceres"];
    let localidadGalicia = ["A Coruña", "Lugo", "Ourense", "Pontevedra"];
    let localidadIslasBaleares = ["Cabrera ", "Formentera", "Ibiza", "Mallorca", "Menorca"];
    let localidadRioja = ["La Rioja"];
    let localidadMurcia = ["Murcia"];
    let localidadNavarra = ["Navarra"];
    let localidadVasco = ["Álava", "Guipuzcoa", "Vizcaya"];
    
   
    let localidadTipos = document.getElementById('Localidad');

    let comunidad = document.getElementById('Comunidad');
    
    let comunidadSeleccionada = comunidad.value;

    switch (comunidadSeleccionada) {

        case "Andalucía":
            autocomplete(localidadTipos, localidadAndalucia);
            break;

        case "Aragón":
            autocomplete(localidadTipos, localidadAragon);
            break;
        case "Asturias":
            autocomplete(localidadTipos, localidadAsturias);
            break;
        case "Canarias":
            autocomplete(localidadTipos, localidadCanarias);
            break;
        case "Cantabria":
            autocomplete(localidadTipos, localidadCantabria);
            break;
        case "Castilla La Mancha":
            autocomplete(localidadTipos, localidadCastillaMancha);
            break;
        case "Castilla y León":
            autocomplete(localidadTipos, localidadCastillaLeon);
            break;
        case "Cataluña":
            autocomplete(localidadTipos, localidadCataluña);
            break;
        case "Comunidad de Madrid":
            autocomplete(localidadTipos, localidadMadrid);
            break;
        case "Comunidad Valenciana":
            autocomplete(localidadTipos, localidadValenciana);
            break;
        case "Extremadura":
            autocomplete(localidadTipos, localidadExtremadura);
            break;
        case "Galicia":
            autocomplete(localidadTipos, localidadGalicia);
            break;
        case "Islas Baleares":
            autocomplete(localidadTipos, localidadIslasBaleares);
            break;
        case "La Rioja":
            autocomplete(localidadTipos, localidadRioja);
            break;
        case "Murcia":
            autocomplete(localidadTipos, localidadMurcia);
            break;
        case "Navarra":
            autocomplete(localidadTipos, localidadNavarra);
            break;
        case "País Vasco":
            autocomplete(localidadTipos, localidadVasco);
            break;

        default:

    }
}

//este metodo comprueba si ha ingresado los nombres
function comprobarName(){

    let regex_name = /^(?=.{1,50}$)[a-zA-Zá-úà-ùä-üÁ-ÚÀ-ÙÄ-ÜñÑçÇ]+(?: [a-zA-Zá-úà-ùä-üÁ-ÚÀ-ÙÄ-ÜñÑçÇ]+)*$/;
    let nombre = $("#Nombre").val();
       
    if (nombre == null || nombre.length == 0 || !regex_name.test(nombre))
    {  
        $("#Nombre").attr("class","form-control is-invalid");           
        cambiarColor('#Nombre', 'red');
        $("#Nombre" ).attr("placeholder","No ha ingresado un nombre");              
    }
    else
    {
        $("#Nombre").attr("class","form-control is-valid");          
             
    }   
}

//este metodo comprueba si ha ingresado los apellidos
function comprobarSurname()
{
    let regex_surname = /^(?=.{1,80}$)[a-zA-Zá-úà-ùä-üÁ-ÚÀ-ÙÄ-ÜñÑçÇ]+(?: [a-zA-Zá-úà-ùä-üÁ-ÚÀ-ÙÄ-ÜñÑçÇ]+)*$/;
    let apellido = $("#Apellidos").val();

    if (apellido == null || apellido.length == 0 || !regex_surname.test(apellido))
    {
        $("#Apellidos").attr("class","form-control is-invalid"); 
        cambiarColor('#Apellidos', 'red');  
        $("#Apellidos" ).attr("placeholder","No ha ingresado un apellido"); 
             
    }
    else
    {       
        $("#Apellidos").attr("class","form-control is-valid");  
              
    }
}

//este metodo comprueba si ha ingresado un DNI,NIE o pasaporte
function comprobarDni()
{
    let regex_dni = new RegExp(/^[XYZxyz]?\d{5,8}[A-Za-z]$/);
    let regex_pasaporteAleman = new RegExp(/(^[CFGHJKLMNPRTVWXY0cfghjklmnprtvwxy])[A-Za-z0-9]{8}$/);
    let regex_pasoporteEspanol = new RegExp(/(^[ZPADzpad])[A-Za-z0-9]{8}$/);
    let regex_pasoportePeruano = new RegExp(/(^[\d]{7})+$/);
    
    let union_regex = new RegExp(regex_dni.source + "|" + regex_pasaporteAleman.source + "|" + regex_pasoporteEspanol.source
        + "|" + regex_pasoportePeruano.source);

    let dni = $("#Dni").val();

    if (dni == null || dni.length == 0 || dni.search(union_regex))
    {
        console.log("hola");
        $("#Dni").attr("class","form-control is-invalid"); 
        cambiarColor('#Dni', 'red');  
        $("#Dni" ).attr("placeholder","No ha ingresado un DNI válido"); 
              
    }
    else
    {       
        $("#Dni").attr("class","form-control is-valid");  
              
    }
}

//este metodo comprueba si ha ingresado un email
function comprobarEmail()
{
    let regex_email = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9-]+\.([a-zA-Z]{2,4})+$/;
    let email = $("#Email").val();

    if (email == null || email.length == 0 || email.search(regex_email))
    {
        $("#Email").attr("class","form-control is-invalid"); 
        cambiarColor('#Email', 'red');  
        $("#Email" ).attr("placeholder","No ha ingresado un correo válido"); 
              
    }
    else
    {       
        $("#Email").attr("class","form-control is-valid");  
               
    }
}

//este metodo comprueba si ha ingresado una contraseña
function comprobarContraseña()
{   
    let contraseña = $("#Password").val();

    if (contraseña == null || contraseña.length == 0)
    {
        $("#Password").attr("class","form-control is-invalid"); 
        cambiarColor('#Password', 'red');  
        $("#Password" ).attr("placeholder","No ha ingresado una contraseña"); 
              
    }
    else
    {       
        $("#Password").attr("class","form-control is-valid");  
              
    }
}

//metodo para mostrar y ocultar contraseñas, se llama desdes el input del html
function mostrarPassword() {  
    var cambio = document.getElementById("Password");

    if (cambio.type == "password") {
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
    } else {
        cambio.type = "password";
    }
}

//este metodo comprueba si ha ingresado una nacionalidad el estudiante
function comprobarNacionalidad()
{
    let countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];

    let nacionalidad = $("#Nacionalidad").val();

    let arrayComprobacion = countries.indexOf(nacionalidad);

    if (arrayComprobacion == -1)
    {
        $("#Nacionalidad").attr("class","form-control is-invalid"); 
        cambiarColor('#Nacionalidad', 'red');  
        $("#Nacionalidad" ).attr("placeholder","No ha ingresado la nacionalidad"); 
               
    }
    else
    {       
        $("#Nacionalidad").attr("class","form-control is-valid");  
              
    }
}

//este metodo comprueba si cambia de nacionalidad el estudiante
function changeNacionalidad() {

    $("#Nacionalidad").change(function () {

        let countries = [
            "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
        ];

        let nacionalidad = $("#Nacionalidad").val();

        let arrayComprobacion = countries.indexOf(nacionalidad);

        if (arrayComprobacion == -1) {
            $("#Nacionalidad").attr("class", "form-control is-invalid");
            cambiarColor('#Nacionalidad', 'red');
            $("#Nacionalidad").attr("placeholder", "No ha ingresado la nacionalidad");
        }
        else {
            $("#Nacionalidad").attr("class", "form-control is-valid");
        }

    });
}

//este metodo comprueba si ha ingresado un país el abuelo
function comprobarPais() {
    let countries = [
        "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
    ];

    let pais = $("#Pais").val();

    let arrayComprobacion = countries.indexOf(pais);

    if (arrayComprobacion == -1) {
        $("#Pais").attr("class", "form-control is-invalid");
        cambiarColor('#Pais', 'red');
        $("#Pais").attr("placeholder", "No ha ingresado un país");

    }
    else {
        $("#Pais").attr("class", "form-control is-valid");

    }
}

//este metodo comprueba si cambia el país el abuelo
function changePais() {

    $("#Pais").change(function () {

        let countries = [
            "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
        ];

        let pais = $("#Pais").val();

        let arrayComprobacion = countries.indexOf(pais);

        if (arrayComprobacion == -1) {
            $("#Pais").attr("class", "form-control is-invalid");
            cambiarColor('#Pais', 'red');
            $("#Pais").attr("placeholder", "No ha ingresado un país");
        }
        else {
            $("#Pais").attr("class", "form-control is-valid");
        }

    });
}

//este metodo comprueba si ha ingresado una comunidad el abuelo
function comprobarComunidad() {
    let listaComunidad = [
        "Andalucía", "Aragón", "Asturias", "Canarias", "Cantabria", "Castilla La Mancha", "Castilla y León", "Cataluña", "Comunidad de Madrid", "Comunidad Valenciana",
        "Extremadura", "Galicia", "Islas Baleares", "La Rioja", "Murcia", "Navarra", "País Vasco"
    ];

    let comunidad = $("#Comunidad").val();

    let arrayComprobacion = listaComunidad.indexOf(comunidad);

    if (arrayComprobacion == -1) {
        $("#Comunidad").attr("class", "form-control is-invalid");
        cambiarColor('#Comunidad', 'red');
        $("#Comunidad").attr("placeholder", "No ha ingresado comunidad");

    }
    else {
        $("#Comunidad").attr("class", "form-control is-valid");

    }
}

//este metodo comprueba si cambia la comunidad el abuelo
function changeComunidad() {

    $("#Comunidad").change(function () {

        let listaComunidad = [
            "Andalucía", "Aragón", "Asturias", "Canarias", "Cantabria", "Castilla La Mancha", "Castilla y León", "Cataluña", "Comunidad de Madrid", "Comunidad Valenciana",
            "Extremadura", "Galicia", "Islas Baleares", "La Rioja", "Murcia", "Navarra", "País Vasco"
        ];

        let comunidad = $("#Comunidad").val();

        let arrayComprobacion = listaComunidad.indexOf(comunidad);

        if (arrayComprobacion == -1) {
            $("#Comunidad").attr("class", "form-control is-invalid");
            cambiarColor('#Comunidad', 'red');
            $("#Comunidad").attr("placeholder", "No ha ingresado comunidad");
        }
        else {
            $("#Comunidad").attr("class", "form-control is-valid");
        }

    });
}

//este metodo comprueba si ha ingresado una localidad el abuelo
function comprobarLocalidad() {
    let listaLocalidad = [
        "Almería", "Cádiz", "Córdoba", "Granada", "Huelva", "Jaén", "Málaga", "Sevilla", //localidadAndalucia
        "Huesca", "Teruel", "Zaragoza", //localidadAragon
        "Asturias", //localidadAsturias
        "Las Palmas", "Santa Cruz de Tenerife", //localidadCanarias
        "Cantabria", //localidadCantabria
        "Albacete", "Ciudad Real", "Cuenca", "Guadalajara", "Toledo", //localidadCastillaMancha
        "Ávila", "Burgos", "León", "Palencia", "Salamanca", "Segovia", "Soria", "Valladolid ", "Zamora", //localidadCastillaLeon
        "Barcelona", "Girona", "Lleida ", "Tarragona", //localidadCataluña
        "Alicante", "Castellón ", "Valencia", //localidadValenciana
        "Madrid", //localidadMadrid
        "Badajoz ", "Cáceres", //localidadExtremadura
        "A Coruña", "Lugo", "Ourense", "Pontevedra", //localidadGalicia
        "Cabrera ", "Formentera", "Ibiza", "Mallorca", "Menorca", //localidadIslasBaleares
        "La Rioja", //localidadRioja
        "Murcia", //localidadMurcia
        "Navarra", //localidadNavarra
        "Álava", "Guipuzcoa", "Vizcaya" //localidadVasco
    ];

    let localidad = $("#Localidad").val();

    let arrayComprobacion = listaLocalidad.indexOf(localidad);

    if (arrayComprobacion == -1) {
        $("#Localidad").attr("class", "form-control is-invalid");
        cambiarColor('#Localidad', 'red');
        $("#Localidad").attr("placeholder", "No ha ingresado localidad");

    }
    else {
        $("#Localidad").attr("class", "form-control is-valid");

    }
}

//este metodo comprueba si cambia la comunidad el abuelo
function changeLocalidad() {

    $("#Localidad").change(function () {

        let listaLocalidad = [
            "Almería", "Cádiz", "Córdoba", "Granada", "Huelva", "Jaén", "Málaga", "Sevilla", //localidadAndalucia
            "Huesca", "Teruel", "Zaragoza", //localidadAragon
            "Asturias", //localidadAsturias
            "Las Palmas", "Santa Cruz de Tenerife", //localidadCanarias
            "Cantabria", //localidadCantabria
            "Albacete", "Ciudad Real", "Cuenca", "Guadalajara", "Toledo", //localidadCastillaMancha
            "Ávila", "Burgos", "León", "Palencia", "Salamanca", "Segovia", "Soria", "Valladolid ", "Zamora", //localidadCastillaLeon
            "Barcelona", "Girona", "Lleida ", "Tarragona", //localidadCataluña
            "Alicante", "Castellón ", "Valencia", //localidadValenciana
            "Madrid", //localidadMadrid
            "Badajoz ", "Cáceres", //localidadExtremadura
            "A Coruña", "Lugo", "Ourense", "Pontevedra", //localidadGalicia
            "Cabrera ", "Formentera", "Ibiza", "Mallorca", "Menorca", //localidadIslasBaleares
            "La Rioja", //localidadRioja
            "Murcia", //localidadMurcia
            "Navarra", //localidadNavarra
            "Álava", "Guipuzcoa", "Vizcaya" //localidadVasco
        ];

        let localidad = $("#Localidad").val();

        let arrayComprobacion = listaLocalidad.indexOf(localidad);

        if (arrayComprobacion == -1) {
            $("#Localidad").attr("class", "form-control is-invalid");
            cambiarColor('#Localidad', 'red');
            $("#Localidad").attr("placeholder", "No ha ingresado localidad");

        }
        else {
            $("#Localidad").attr("class", "form-control is-valid");

        }

    });
}


//este metodo comprueba si ha ingresado un CP el abuelo
function comprobarCodigoPostal() {

    let regex_codigoPostal = /^[\d]+$/;
    let codigoPostal = $("#CodigoPostal").val();

    if (codigoPostal == null || codigoPostal.length == 0 || !regex_codigoPostal.test(codigoPostal)) {
        $("#CodigoPostal").attr("class", "form-control is-invalid");
        cambiarColor('#CodigoPostal', 'red');
        $("#CodigoPostal").attr("placeholder", "No ha ingresado un CP");

    }
    else {
        $("#CodigoPostal").attr("class", "form-control is-valid");

    }
}

//este metodo comprueba si ha ingresado una dirección el abuelo
function comprobarDireccion() {

    let regex_direccion = /^[^$%&|<>#|\s]+(?: [\wá-úà-ùä-üÁ-ÚÀ-ÙÄ-ÜñÑçÇ|(),;.·:_ºª'|-]+)*$/;  
    let direccion = $("#Direccion").val();

    if (direccion == null || direccion.length == 0 || !regex_direccion.test(direccion)) {
        $("#Direccion").attr("class", "form-control is-invalid");
        cambiarColor('#Direccion', 'red');
        $("#Direccion").attr("placeholder", "No ha ingresado una dirección");

    }
    else {
        $("#Direccion").attr("class", "form-control is-valid");

    }
}

//este metodo comprueba si ha ingresado una fecha de nacimiento
function comprobarFecha()
{  
    let fecha = $("#FechaNacimiento").val(); 

    if (fecha == null || fecha.length == 0)
    {
        $("#FechaNacimiento").attr("class","form-control is-invalid"); 
        cambiarColor('#FechaNacimiento', 'red');  
        $("#FechaNacimiento").attr("placeholder", "No ha ingresado una fecha válida");
        
    }
    else
    {       
        $("#FechaNacimiento").attr("class", "form-control is-valid"); 
        
    }
}

//este metodo comprueba si cambia la fecha de nacimiento
function changeFecha() {

    $("#FechaNacimiento").change(function () {

        let fecha = $("#FechaNacimiento").val();

        if (fecha == null || fecha.length == 0) {

            $("#FechaNacimiento").attr("class", "form-control is-invalid");
            cambiarColor('#FechaNacimiento', 'red');
            $("#FechaNacimiento").attr("placeholder", "No ha ingresado una fecha válida");

        }
        else {
            $("#FechaNacimiento").attr("class", "form-control is-valid");

        }
    });
}

//este metodo comprueba si ha ingresado un telefono
function comprobarTelefono()
{
    let regex_telefonoEspanol = /(^[6789])\d{8}$/;

    let regex_internacionalSinEspacio = new RegExp(/^\+\d{2,3}\d{9,11}$/); //+2 al final debido a que hay numero que tienen 9 o 11 despues del espacio
    let regex_internacionalConEspacio = new RegExp(/^\+\d{2,3}\s\d{9,11}$/); //+2 al final debido a que hay numero que tienen 9 o 11 despues del espacio

    let union_regex = new RegExp(regex_telefonoEspanol.source + "|" + regex_internacionalSinEspacio.source + "|"
        + regex_internacionalConEspacio.source);

    let telefono = $("#Telefono").val();

    if (telefono == null || telefono.length == 0 || telefono.search(union_regex))
    {
        $("#Telefono").attr("class","form-control is-invalid"); 
        cambiarColor('#Telefono', 'red');  
        $("#Telefono" ).attr("placeholder","No ha ingresado un teléfono válido"); 
               
    }
    else
    {       
        $("#Telefono").attr("class","form-control is-valid");  
            
    }
}

//este metodo comprueba si ha ingresado el nivel de estudios el estudiante
function comprobarEstudios()
{
    let estudios = $("#Estudios").val();

    if (estudios == null || estudios == 0)
    {
        $("#Estudios").attr("class","form-control is-invalid"); 
        cambiarColor('#Estudios', 'red');  
        $("#Estudios option[value=0]").text("No ha ingresado una referencia"); 
        cssRojo('#Estudios');     
    }
    else
    {       
        $("#Estudios").attr("class","form-control is-valid");  
               
    }
}

//este metodo comprueba si cambia el nivel de estudios el estudiante
function changeEstudios() {

    $("#Estudios").change(function () {

        let estudios = $("#Estudios").val();

        if (estudios == null || estudios == 0) {

            $("#Estudios").attr("class", "form-control is-invalid");
            $("#Estudios option[value=0]").text("No ha ingresado una referencia");
            cssRojo('#Estudios');

        }
        else {
            $("#Estudios").attr("class", "form-control is-valid");

        }
    });
}

//este metodo comprueba si ha ingresado el genero
function comprobarSexo()
{
    let sexo = $("#Sexo").val();

    if (sexo == null || sexo == 0)
    {
        $("#Sexo").attr("class","form-control is-invalid"); 
        cambiarColor('#Sexo', 'red');  
        $("#Sexo option[value=0]").text("No ha ingresado su género");
        cssRojo('#Sexo');
              
    }
    else
    {       
        $("#Sexo").attr("class","form-control is-valid");  
             
    }
}

//este metodo comprueba si cambia de genero
function changeSexo(){

    $("#Sexo").change(function () {

        let sexo = $("#Sexo").val();

        if (sexo == null || sexo == 0) {

            $("#Sexo").attr("class", "form-control is-invalid");
            $("#Sexo option[value=0]").text("No ha ingresado su género");
            cssRojo('#Sexo');

        }
        else {
            $("#Sexo").attr("class", "form-control is-valid");

        }
    });

}

//metodo para añdir una class a un optionselect, luego por css todos los que tengan esta class se le pinta en rojo
function cssRojo(inputs) {
    $(inputs).each(function () {

        if ($(this).val() == 0) {

            $(this).addClass('select-rojo');
        }
        else {
            $(this).removeClass('select-rojo');
        }
    });
}

//este metodo añade un estilo a un input, en este caso se lo damos al placeholder para poder pintar en rojo
function cambiarColor(inputs, color_dinamico) {
    $("body").append("<style>" + inputs + "::placeholder{color:" +  color_dinamico + "}</style>")
}

//este metodo enviara el formulario de registro
function enviarFormulario(){
    if(Registrar()){
        $('#btnvalidar').submit();
    }
}

//metodo para los campos del autocompletado
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/

                    //cada vez que seleccione una opcion, llamaremos de nuevo a las funciones de comprobacion
                    //Estudiante
                    $("#Nacionalidadautocomplete-list").click(function () {
                        comprobarNacionalidad();
                    });

                    //Abuelo
                    //Paisautocomplete-list, al clicar cualquier opcion del pais
                    $("#Paisautocomplete-list").click(function () {
                        comprobarPais();
                    });
                    
                    //Comunidadautocomplete-list, al clicar cualquier opcion de la comunidad
                    $("#Comunidadautocomplete-list").click(function () {
                        comprobarComunidad();
                        autocompletadoLocalidad();
                    });

                    $("#Localidadautocomplete-list").click(function () {
                        comprobarLocalidad();
                    });

                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });

  

    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}