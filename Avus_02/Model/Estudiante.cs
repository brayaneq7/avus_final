﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("estudiante")]
    public partial class Estudiante
    {
        public Estudiante()
        {
            OpinionesAbuelo = new HashSet<OpinionesAbuelo>();
            OpinionesEstudiante = new HashSet<OpinionesEstudiante>();
            Solicitud = new HashSet<Solicitud>();
        }

        [Key]
        [Column("id_estudiante")]
        public int IdEstudiante { get; set; }
        [Column("email")]
        [StringLength(150)]
        public string Email { get; set; }
        [Column("password")]
        [StringLength(150)]
        public string Password { get; set; }
        [Column("nombre")]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Column("apellidos")]
        [StringLength(100)]
        public string Apellidos { get; set; }
        [Column("dni")]
        [StringLength(50)]
        public string Dni { get; set; }
        [Column("descripcion")]
        [StringLength(500)]
        public string Descripcion { get; set; }
        [Column("nacionalidad")]
        public int? Nacionalidad { get; set; }
        [Column("foto_perfil")]
        [StringLength(300)]
        public string FotoPerfil { get; set; }
        [Column("fecha_nacimiento", TypeName = "date")]
        public DateTime? FechaNacimiento { get; set; }
        [Column("sexo")]
        public int? Sexo { get; set; }
        [Column("telefono")]
        [StringLength(50)]
        public string Telefono { get; set; }
        [Column("estudios")]
        public int? Estudios { get; set; }

        [InverseProperty("IdEstudianteNavigation")]
        public ICollection<OpinionesAbuelo> OpinionesAbuelo { get; set; }
        [InverseProperty("IdEstudianteNavigation")]
        public ICollection<OpinionesEstudiante> OpinionesEstudiante { get; set; }
        [InverseProperty("IdEstudianteNavigation")]
        public ICollection<Solicitud> Solicitud { get; set; }
    }
}
