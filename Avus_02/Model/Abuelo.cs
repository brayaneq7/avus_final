﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("abuelo")]
    public partial class Abuelo
    {

        public Abuelo()
        {
            Habitacion = new HashSet<Habitacion>();
            OpinionesAbuelo = new HashSet<OpinionesAbuelo>();
            OpinionesEstudiante = new HashSet<OpinionesEstudiante>();
        }

        [Key]
        [Column("id_abuelo")]
        public int IdAbuelo { get; set; }
        [Column("email")]
        [StringLength(150)]
        public string Email { get; set; }
        [Column("password")]
        [StringLength(150)]
        public string Password { get; set; }
        [Column("nombre")]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Column("apellidos")]
        [StringLength(100)]
        public string Apellidos { get; set; }
        [Column("dni")]
        [StringLength(50)]
        public string Dni { get; set; }
        [Column("descripcion")]
        [StringLength(500)]
        public string Descripcion { get; set; }
        [Column("nacionalidad")]
        public int? Nacionalidad { get; set; }
        [Column("foto_perfil")]
        [StringLength(300)]
        public string FotoPerfil { get; set; }
        [Column("fecha_nacimiento", TypeName = "date")]
        public DateTime? FechaNacimiento { get; set; }
        [Column("sexo")]
        public int? Sexo { get; set; }
        [Column("telefono")]
        [StringLength(50)]
        public string Telefono { get; set; }
        [Column("pais")]
        public int? Pais { get; set; }
        [Column("comunidad")]
        public int? Comunidad { get; set; }
        [Column("localidad")]
        public int? Localidad { get; set; }
        [Column("direccion")]
        [StringLength(150)]
        public string Direccion { get; set; }
        [Column("codigo_postal")]
        public int? CodigoPostal { get; set; }

        [InverseProperty("IdAbueloNavigation")]
        public ICollection<Habitacion> Habitacion { get; set; }
        [InverseProperty("IdAbueloNavigation")]
        public ICollection<OpinionesAbuelo> OpinionesAbuelo { get; set; }
        [InverseProperty("IdAbueloNavigation")]
        public ICollection<OpinionesEstudiante> OpinionesEstudiante { get; set; }

        
    }
}
