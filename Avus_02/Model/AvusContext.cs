﻿﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Avus_02.Model
{
    public partial class AvusContext : DbContext
    {
        public AvusContext()
        {
        }

        public AvusContext(DbContextOptions<AvusContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Abuelo> Abuelo { get; set; }
        public virtual DbSet<Estudiante> Estudiante { get; set; }
        public virtual DbSet<FotoHabitacion> FotoHabitacion { get; set; }
        public virtual DbSet<Habitacion> Habitacion { get; set; }
        public virtual DbSet<OpinionesAbuelo> OpinionesAbuelo { get; set; }
        public virtual DbSet<OpinionesEstudiante> OpinionesEstudiante { get; set; }
        public virtual DbSet<Solicitud> Solicitud { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           //optionsBuilder.UseSqlServer("Server=localhost;Database=master;Trusted_Connection=True;");
           optionsBuilder.UseSqlServer("Server=MSI;Database=bbddAvus;Trusted_Connection=True;");
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Abuelo>(entity =>
            {
                entity.Property(e => e.Apellidos).IsUnicode(false);

                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.Direccion).IsUnicode(false);

                entity.Property(e => e.Dni).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FotoPerfil).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Telefono).IsUnicode(false);
            });

            modelBuilder.Entity<Estudiante>(entity =>
            {
                entity.Property(e => e.Apellidos).IsUnicode(false);

                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.Dni).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FotoPerfil).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Telefono).IsUnicode(false);
            });

            modelBuilder.Entity<FotoHabitacion>(entity =>
            {
                entity.Property(e => e.Foto).IsUnicode(false);

                entity.HasOne(d => d.IdHabitacionNavigation)
                    .WithMany(p => p.FotoHabitacion)
                    .HasForeignKey(d => d.IdHabitacion)
                    .HasConstraintName("FK_foto_habitacion_habitacion");
            });

            modelBuilder.Entity<Habitacion>(entity =>
            {
                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.HasOne(d => d.IdAbueloNavigation)
                    .WithMany(p => p.Habitacion)
                    .HasForeignKey(d => d.IdAbuelo)
                    .HasConstraintName("FK_habitacion_abuelo");
            });

            modelBuilder.Entity<OpinionesAbuelo>(entity =>
            {
                entity.Property(e => e.Opinion).IsUnicode(false);

                entity.HasOne(d => d.IdAbueloNavigation)
                    .WithMany(p => p.OpinionesAbuelo)
                    .HasForeignKey(d => d.IdAbuelo)
                    .HasConstraintName("FK_opiniones_abuelo_abuelo");

                entity.HasOne(d => d.IdEstudianteNavigation)
                    .WithMany(p => p.OpinionesAbuelo)
                    .HasForeignKey(d => d.IdEstudiante)
                    .HasConstraintName("FK_opiniones_abuelo_estudiante");
            });

            modelBuilder.Entity<OpinionesEstudiante>(entity =>
            {
                entity.Property(e => e.Opinion).IsUnicode(false);

                entity.HasOne(d => d.IdAbueloNavigation)
                    .WithMany(p => p.OpinionesEstudiante)
                    .HasForeignKey(d => d.IdAbuelo)
                    .HasConstraintName("FK_opiniones_estudiante_abuelo");

                entity.HasOne(d => d.IdEstudianteNavigation)
                    .WithMany(p => p.OpinionesEstudiante)
                    .HasForeignKey(d => d.IdEstudiante)
                    .HasConstraintName("FK_opiniones_estudiante_estudiante");
            });

            modelBuilder.Entity<Solicitud>(entity =>
            {
                entity.HasOne(d => d.IdEstudianteNavigation)
                    .WithMany(p => p.Solicitud)
                    .HasForeignKey(d => d.IdEstudiante)
                    .HasConstraintName("FK_solicitud_estudiante");

                entity.HasOne(d => d.IdHabitacionNavigation)
                    .WithMany(p => p.Solicitud)
                    .HasForeignKey(d => d.IdHabitacion)
                    .HasConstraintName("FK_solicitud_habitacion");
            });
        }
    }
}
