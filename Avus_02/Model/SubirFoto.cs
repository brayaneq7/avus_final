﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avus_02.Model
{
    public class SubirFoto
    {
        public IFormFile MyImage { set; get; }
    }
}
