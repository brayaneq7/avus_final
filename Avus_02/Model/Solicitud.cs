﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("solicitud")]
    public partial class Solicitud
    {
        [Key]
        [Column("id_solicitud")]
        public int IdSolicitud { get; set; }
        [Column("fecha_solicitud", TypeName = "date")]
        public DateTime? FechaSolicitud { get; set; }
        [Column("aceptacion")]
        public int? Aceptacion { get; set; }
        [Column("fecha_primer_dia", TypeName = "date")]
        public DateTime? FechaPrimerDia { get; set; }
        [Column("fecha_ultimo_dia", TypeName = "date")]
        public DateTime? FechaUltimoDia { get; set; }
        [Column("id_estudiante")]
        public int? IdEstudiante { get; set; }
        [Column("id_habitacion")]
        public int? IdHabitacion { get; set; }

        [ForeignKey("IdEstudiante")]
        [InverseProperty("Solicitud")]
        public Estudiante IdEstudianteNavigation { get; set; }
        [ForeignKey("IdHabitacion")]
        [InverseProperty("Solicitud")]
        public Habitacion IdHabitacionNavigation { get; set; }
    }
}
