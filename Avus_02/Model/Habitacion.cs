﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("habitacion")]
    public partial class Habitacion
    {
        public Habitacion()
        {
            FotoHabitacion = new HashSet<FotoHabitacion>();
            Solicitud = new HashSet<Solicitud>();
        }

        [Key]
        [Column("id_habitacion")]
        public int IdHabitacion { get; set; }
        [Column("descripcion")]
        [StringLength(500)]
        public string Descripcion { get; set; }
        [Column("fumar")]
        public int? Fumar { get; set; }
        [Column("wifi")]
        public int? Wifi { get; set; }
        [Column("mascotas")]
        public int? Mascotas { get; set; }
        [Column("invitados")]
        public int? Invitados { get; set; }
        [Column("disponible")]
        public int? Disponible { get; set; }
        [Column("id_abuelo")]
        public int? IdAbuelo { get; set; }

        [ForeignKey("IdAbuelo")]
        [InverseProperty("Habitacion")]
        public Abuelo IdAbueloNavigation { get; set; }
        [InverseProperty("IdHabitacionNavigation")]
        public ICollection<FotoHabitacion> FotoHabitacion { get; set; }
        [InverseProperty("IdHabitacionNavigation")]
        public ICollection<Solicitud> Solicitud { get; set; }
    }
}
