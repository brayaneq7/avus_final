﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("foto_habitacion")]
    public partial class FotoHabitacion
    {
        [Key]
        [Column("id_foto_habitacion")]
        public int IdFotoHabitacion { get; set; }
        [Column("foto")]
        [StringLength(300)]
        public string Foto { get; set; }
        [Column("id_habitacion")]
        public int? IdHabitacion { get; set; }

        [ForeignKey("IdHabitacion")]
        [InverseProperty("FotoHabitacion")]
        public Habitacion IdHabitacionNavigation { get; set; }
    }
}
