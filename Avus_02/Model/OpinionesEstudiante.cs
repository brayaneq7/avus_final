﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("opiniones_estudiante")]
    public partial class OpinionesEstudiante
    {
        [Key]
        [Column("id_opiniones_estudiante")]
        public int IdOpinionesEstudiante { get; set; }
        [Column("opinion")]
        [StringLength(500)]
        public string Opinion { get; set; }
        [Column("id_abuelo")]
        public int? IdAbuelo { get; set; }
        [Column("id_estudiante")]
        public int? IdEstudiante { get; set; }

        [ForeignKey("IdAbuelo")]
        [InverseProperty("OpinionesEstudiante")]
        public Abuelo IdAbueloNavigation { get; set; }
        [ForeignKey("IdEstudiante")]
        [InverseProperty("OpinionesEstudiante")]
        public Estudiante IdEstudianteNavigation { get; set; }
    }
}
