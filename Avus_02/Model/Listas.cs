﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;

namespace Avus_02.Model
{
    public class Listas
    {
        public static string[] Countries = new string[]
        {
            "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue"
        };

        public static Dictionary<string, int> Comunidades = new Dictionary<string, int>{

                { "Andalucia",0},
                {"Aragón",1},
                {"Canarias",2},
                {"Cantabria",3},
                {"Castilla y León",4},
                {"Castilla-La Mancha",5},
                {"Cataluña",6},
                {"Ceuta",7},
                {"Comunidad Valenciana",8},
                {"Comunidad de Madrid",9},
                {"Extremadura",10},
                {"Galicia",11},
                {"Islas Baleares",12},
                {"La Rioja",13},
                {"Melilla",14},
                {"Navarra",15},
                {"País Vasco",16},
                {"Principado de Asturias",17},
                {"Región de Murcia",18}
        };

        public static string[][] Provincias = new string[][]{

                new string[] {"Almeria","Cadiz","Cordoba","Granada","Huelva","Jaen","Malaga","Sevilla"},
                new string[] {"Huesca","Teruel","Zaragoza"},
                new string[] {"Las Palmas","Santa Cruz de Tenerife"},
                new string[] {"Cantabria"},
                new string[] {"Avila","Burgos","Leon","Palencia","Salamanca","Segovia","Soria","Valladolid","Zamora"},
                new string[] {"Albacete","Ciudad Real","Cuenca","Guadalajara","Toledo"},
                new string[] {"Barcelona","Girona","Lleida","Tarragona"},
                new string[] {"Ceuta"},
                new string[] {"Alicante","Castellon","Valencia"},
                new string[] {"Madrid"},
                new string[] {"Badajoz","Caceres"},
                new string[] {"A Coruña","Lugo","Ourense","Pontevedra"},
                new string[] {"Baleares"},
                new string[] {"La Rioja"},
                new string[] {"Melilla"},
                new string[] {"Navarra"},
                new string[] {"Alava","Guipuzcoa","Vizcaya"},
                new string[] {"Asturias"},
                new string[] {"Murcia"}
        };

        public static string[] Barrios = new string[]{
                "el Raval",
                "el Gòtic",
                "la Barceloneta",
                "Sant Pere, Santa Caterina i la Ribera",
                "el Fort Pienc",
                "la Sagrada Família",
                "la Dreta de l'Eixample",
                "l'Antiga Esquerra de l'Eixample",
                "la Nova Esquerra de l'Eixample",
                "Sant Antoni",
                "el Poble Sec",
                "la Marina del Prat Vermell",
                "la Marina de Port",
                "la Font de la Guatlla",
                "Hostafrancs",
                "la Bordeta",
                "Sants-Badal",
                "Sants",
                "les Corts",
                "la Maternitat i Sant Ramon",
                "Pedralbes",
                "Vallvidrera, el Tibidabo i les Planes",
                "Sarrià",
                "les Tres Torres",
                "Sant Gervasi-Bonanova",
                "Sant Gervasi-Galvany",
                "el Putget i Farró",
                "Vallcarca i els Penitents",
                "el Coll",
                "la Salut",
                "Vila de Gràcia",
                "el Camp d'en Grassot i Gràcia Nova",
                "el Baix Guinardó",
                "Can Baró",
                "el Guinardó",
                "la Font d'en Fargues",
                "el Carmel",
                "la Teixonera",
                "Sant Genís dels Agudells",
                "Montbau",
                "la Vall d'Hebron",
                "la Clota",
                "Horta",
                "Vilapicina i la Torre Llobeta",
                "Porta",
                "el Turó de la Peira",
                "Can Peguera",
                "la Guineueta",
                "Canyelles",
                "les Roquetes",
                "Verdun",
                "la Prosperitat",
                "la Trinitat Nova",
                "Torre Baró",
                "Ciutat Meridiana",
                "Vallbona",
                "la Trinitat Vella",
                "Baró de Viver",
                "el Bon Pastor",
                "Sant Andreu",
                "la Sagrera",
                "el Congrés i els Indians",
                "Navas",
                "el Camp de l'Arpa del Clot",
                "el Clot",
                "el Parc i la Llacuna del Poblenou",
                "la Vila Olímpica del Poblenou",
                "el Poblenou",
                "Diagonal Mar i el Front Marítim del Poblenou",
                "el Besòs i el Maresme",
                "Provençals del Poblenou",
                "Sant Martí de Provençals",
                "la Verneda i la Pau"
        };
    }
}