﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Avus_02.Model
{
    [Table("opiniones_abuelo")]
    public partial class OpinionesAbuelo
    {
        [Key]
        [Column("id_opiniones_abuelo")]
        public int IdOpinionesAbuelo { get; set; }
        [Column("opinion")]
        [StringLength(500)]
        public string Opinion { get; set; }
        [Column("id_estudiante")]
        public int? IdEstudiante { get; set; }
        [Column("id_abuelo")]
        public int? IdAbuelo { get; set; }

        [ForeignKey("IdAbuelo")]
        [InverseProperty("OpinionesAbuelo")]
        public Abuelo IdAbueloNavigation { get; set; }
        [ForeignKey("IdEstudiante")]
        [InverseProperty("OpinionesAbuelo")]
        public Estudiante IdEstudianteNavigation { get; set; }
    }
}
